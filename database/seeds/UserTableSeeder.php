<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();
        DB::table('users')->insert([
            [
            'id'=>2,
            'name' => 'Admin',
            'username' => 'Admin Guy',
            'btc_address' => '18VEaAWRtsjvbgT6YUtfhBatEmcqYhULHi',
            'email' => 'barjebernard@gmail.com',
            'password' => bcrypt('admin123..'),
            'is_superadmin' => true,
            'is_activated' => true,
            'phone' => '3434234345',
            'verified' => true
                ],

            [
                'id'=>1,
                'name'=>'Martins',
                'username'=>'martins',
                'email'=>'dennis336278@gmail.com',
                'password'=> bcrypt('111111'),
                'is_superadmin'=> true,
                'is_activated'=>true,
                'btc_address' => '18VEaAWRtsjvbgT6YUtfhBatEmcqYhULHi',
                'phone'=>'0329290390',
                'verified'=>true
            ]
        ]);
    }
}
