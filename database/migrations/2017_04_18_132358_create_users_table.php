 <?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email');
            $table->string('old_email')->nullable();
            $table->string('password');
            $table->string('old_password')->nullable();
            $table->string('phone')->unique();
            $table->integer('phase')->default(0);
            $table->integer('level')->default(0);
            $table->integer('recycle_count')->default(0);
            $table->string('referrer')->default(1);
            $table->integer('left')->nullable();
            $table->integer('right')->nullable();
            $table->boolean('is_admin')->default(false);
            $table->boolean('loan_account')->default(false);
            $table->boolean('is_superadmin')->default(false);
            $table->boolean('is_activated')->default(false);
            $table->integer('earnings')->nullable();
            $table->integer('help_count')->nullable();
            $table->integer('received_count')->nullable();
            $table->integer('payments')->nullable();
            $table->string('btc_address')->nullable();
            $table->string('old_btc_address')->nullable();
            $table->tinyInteger('verified')->default(0); // this column will be a TINYINT with a default value of 0 , [0 for false & 1 for true i.e. verified]
            $table->string('email_token')->nullable(); // this column will be a VARCHAR with no default value and will also BE NULLABLE
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}