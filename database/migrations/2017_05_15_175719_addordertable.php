<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addordertable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('order_amount');
            $table->integer('amount_paid');
            $table->integer('btc_address');
            $table->string('trans_id');
            $table->integer('payment_method');
            $table->integer('rate')->nullable();
            $table->text('message')->nullable();
            $table->string('order_type')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');

    }


}

