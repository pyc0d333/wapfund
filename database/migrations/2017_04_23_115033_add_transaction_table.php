<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table){
            $table->increments('id');
            $table->string('tx_hash');
            $table->integer('user_id');
            $table->integer('amount_paid');
            $table->integer('expected_amount')->nullable();
            $table->boolean('approved')->default(false);
            $table->integer('level');
            $table->integer('phase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
