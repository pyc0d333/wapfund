<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="bitycom, bitycom.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <link rel="stylesheet" href="<?php echo e(asset('assets/bootstrap-material-design-font/css/material.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/et-line-font-plugin/style.css')); ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="<?php echo e(asset('assets/web/assets/mobirise-icons/mobirise-icons.css')); ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&subset=latin">
    <link rel="stylesheet" href="<?php echo e(asset('assets/tether/tether.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/dropdown/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/socicon/css/styles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/animate.css/animate.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/mobirise/css/mbr-additional.css')); ?>" type="text/css">



</head>
<body>
<section id="ext_menu-m">

    <nav class="navbar navbar-dropdown bg-color teal navbar-fixed-top">
        <div class="container">

            <div class="mbr-table">
                <div class="mbr-table-cell">
                    <div class="navbar-brand">
                        <a href="https://mobirise.com" class="navbar-logo">
                            <img src="assets/images/logo.png"></a>
                        <a class="navbar-caption" href="/">Bitycom</a>
                    </div>
                </div>
                <div class="mbr-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="hamburger-icon"></div>
                    </button>

                    <ul class="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar">
                        <li class="nav-item"><a class="nav-link link" href="/">Home</a></li>
                        <li class="nav-item"><a class="nav-link link" href="#btc">Buy/Sell BTC</a></li>
                        <li class="nav-item"><a class="nav-link link" href="<?php echo e(route('knowledgebase')); ?>">Tutorials</a></li>
                        <li class="nav-item"><a class="nav-link link" href="/faq">Faq</a></li>
                        <li class="nav-item"><a class="nav-link link" href="<?php echo e(url('/about')); ?>">About</a></li>
                        <li class="nav-item"><a class="nav-link link" href="<?php echo e(url('/works')); ?>">How it works</a></li>
                        <?php if(!Auth::check()): ?>
                        <li class="nav-item"><a class="nav-link link" href="<?php echo e(route('register')); ?>">Sign up</a></li>
                            <li class="nav-item"><a class="nav-link link" href="<?php echo e(route('login')); ?>">Login</a></li></ul>
                    <?php else: ?>
                        <li class="nav-item"><a class="nav-link link" href="<?php echo e(route('home')); ?>">Dashboard</a></li>
                        <li class="nav-item">      <a href="<?php echo e(url('/logout')); ?>"
                                                      onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Logout
                            </a>

                            <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                <?php echo e(csrf_field()); ?>

                            </form>  </li>
                        </ul>

                    <?php endif; ?>
                        <button hidden="" style="color: white" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="close-icon" style="color: white"></div>
                    </button>

                </div>
            </div>

        </div>
    </nav>

</section>
<?php echo $__env->yieldContent('content'); ?>

<section class="mbr-section mbr-section-md-padding mbr-footer footer1" id="contacts1-9" style="background-color: rgb(46, 46, 46); padding-top: 10px; padding-bottom: 0px;">

    <div class="container">
        <div class="row">
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <a href="<?php echo e(url('/privacy')); ?>"><strong>Privacy Policy</strong></a>

            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <a href="<?php echo e(url('/privacy')); ?>"><strong>Terms of Service</strong></a>

            </div> <div class="mbr-footer-content col-xs-12 col-md-3">

                <a href="<?php echo e(url('/works')); ?>"><strong>How it Works</strong></a>
            </div> <div class="mbr-footer-content col-xs-12 col-md-3">

                <a href="<?php echo e(route('knowledgebase')); ?>"><strong>Knowledgebase</strong></a>
            </div>

        </div>
    </div>
</section>

<footer class="mbr-small-footer mbr-section mbr-section-nopadding" id="footer1-a" style="background-color: rgb(50, 50, 50); padding-top: 1.75rem; padding-bottom: 1.75rem;">

    <div class="container">
        <p class="text-xs-center">Copyright (c) 2017 Bitycom.</p>
    </div>
</footer>


<script src="<?php echo e(asset('assets/web/assets/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/tether/tether.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/dropdown/js/script.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/touch-swipe/jquery.touch-swipe.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/smooth-scroll/smooth-scroll.js')); ?>"></script>
<script src="<?php echo e(asset('assets/viewport-checker/jquery.viewportchecker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/jarallax/jarallax.js')); ?>"></script>
<script src="<?php echo e(asset('assets/theme/js/script.js')); ?>"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/59c79480c28eca75e4621e6d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<input name="animation" type="hidden">
</body>
</html>