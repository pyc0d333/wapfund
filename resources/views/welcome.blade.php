@extends('layouts.dash_assets')
@section('body')


    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-dark">
                    <div class="panel-body">
                        <h3>You have successfully signed up, please check your email to verify your account</h3>
                     <p>Check your spam folder if you can't find it</p>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection