@extends('layouts.frontend')
@section('content')



    <section class="mbr-section article mbr-section__container mbr-after-navbar" id="content2-e" style="background-color: rgb(255, 255, 255); padding-top: 140px; padding-bottom: 140px;">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 lead">
                    <h3>Verified and Trusted Exchanges</h3>

                    <ul>
                        @foreach($exchanges as $exchange)
                            <li style="color: black"><span class="pull-left">{{$exchange->title}}</span> <span class="pull-right badge badge-primary"><a href="{{$exchange->link}}" target="_blank">{{$exchange->link}}</a></span></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </section>

@endsection
