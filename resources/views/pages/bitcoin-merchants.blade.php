
@extends('layouts.auth-layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">BUY AND SELL YOUR BITCOINS</h6>
                            <a href="/" class="btn btn-primary">Home</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table  class="table  top-countries" >
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/gb.svg" alt="country" class="avatar">
                                                    </td>
                                                    <td>
                                                        United Kingdom
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/my.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Malaysia
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/au.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Australia
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/us.svg"  alt="country">
                                                    </td>
                                                    <td>
                                                        United States
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/fr.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        France
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/jp.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        japan
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/it.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Italy
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table  class="table  top-countries" >
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/gb.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        United Kingdom
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/my.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Malaysia
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/au.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Australia
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/us.svg"  alt="country">
                                                    </td>
                                                    <td>
                                                        United States
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/fr.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        France
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/jp.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        japan
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/it.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Italy
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table  class="table  top-countries" >
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/gb.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        United Kingdom
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/my.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Malaysia
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/au.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Australia
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/us.svg"  alt="country">
                                                    </td>
                                                    <td>
                                                        United States
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/fr.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        France
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/jp.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        japan
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/it.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Italy
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table  class="table  top-countries" >
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/gb.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        United Kingdom
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/my.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Malaysia
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/au.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Australia
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/us.svg"  alt="country">
                                                    </td>
                                                    <td>
                                                        United States
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/fr.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        France
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/jp.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        japan
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="dist/img/country/it.svg" alt="country">
                                                    </td>
                                                    <td>
                                                        Italy
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection()