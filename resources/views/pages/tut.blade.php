@extends('layouts.frontend')
@section('content')

    <section class="mbr-section article mbr-section__container mbr-after-navbar" id="content2-e" style="background-color: rgb(255, 255, 255); padding-top: 140px; padding-bottom: 140px;">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 lead">
                    <h3>{{$tut->title}}</h3>
                    <br>

                   <div id="summernote">
                       {!! $tut->content !!}
                   </div>

                </div>
            </div>
        </div>

    </section>
    <script>
        $("#summernote").code().text();
    </script>
@endsection
