<!DOCTYPE html>
<html lang="en">
<head>
@include('partials.head')
<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->




<div class="wrapper slide-nav-toggle">

    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block mr-20 pull-left txt-danger" href="javascript:void(0);"><i class="fa fa-bars"></i></a>
        {{--<a href="#"><img class="brand-img pull-left" height="50px" width="60px" src="" alt=""/></a>--}}

        <ul class="nav navbar-right top-nav pull-right">
            <li>
                <a id="open_right_sideba" href="/" style="color: white">Home</a>
            </li>
            <li>
                <a id="open_right_sidear" href="/faq" style="color: white">Faqs</a>
            </li>
            <li>
                <a id="open_right_sidebar" href="/works" style="color: white">How it works</a>
            </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-user"></i><span class="user-online-status"></span></a>
                <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <li>
                        <a href="/profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>

                        <a href="{{ url('/logout') }}" class="txt-danger"
                           onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /Top Menu Items -->

@include('partials.sidebar')


<!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            @yield('content')



        </div>
        <!-- /Main Content -->

    </div>
</div>
<!-- /#wrapper -->

<!-- JavaScript -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/59c79480c28eca75e4621e6d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@include('partials.scripts')

</body>

</html>