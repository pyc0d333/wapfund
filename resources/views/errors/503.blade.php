@extends('layouts.dash_assets')

@section('body')
    <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
            margin-left: 150px;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
    <div class="page page-core page-login">
        <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred">W</span>APFUND</h3></div>
        <div class="container">
            <div class="content">
                <div class="title">We are fixing the site </div>
                <div class="title">Please come back later </div>
            </div>
        </div>

        </div>
    </div>

    @endsection