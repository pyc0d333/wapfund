@extends('layouts.frontend')
@section('content')

  <section class="mbr-slider mbr-section mbr-section__container carousel slide mbr-section-nopadding mbr-after-navbar" data-ride="carousel" data-keyboard="false" data-wrap="true" data-pause="false" data-interval="5000" id="slider-o">
        <div>
            <div>
                <div>
                    <ol class="carousel-indicators">
                        <li data-app-prevent-settings="" data-target="#slider-o" data-slide-to="0" class="active"></li>
                        <li data-app-prevent-settings="" data-target="#slider-o" data-slide-to="1"></li>
                        <li data-app-prevent-settings="" data-target="#slider-o" data-slide-to="2"></li>
                        <li data-app-prevent-settings="" data-target="#slider-o" class="" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full active" data-bg-video-slide="false" style="background-image: url('http://res.cloudinary.com/py/image/upload/v1503342418/WAP_HYJTJT_ynoebv.jpg');">
                            <div class="mbr-table-cell">
                                <div class="mbr-overlay">
                                </div>
                                <div class="container-slide container">

                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 text-xs-center">
                                            <h1 class="mbr-section-title display">ARE YOU SUCCESSFUL?
                                            </h1>
                                            <p class="mbr-section-lead lea">
                                                <strong>Success Is Measured By The Number Of People You Made Successful
                                                Render A Helping Hand To Someone Today By
                                                    Bringing Them On Board To Be A Part Of This Financial Empowerment.</strong>
                                            </p>
                                            <div class="mbr-section-btn"><a class="btn btn-lg btn-success" href="{{route('register')}}">Join Us</a>
                                                <a class="btn btn-lg btn-white btn-white-outline" href="{{route('login')}}">Login</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(http://res.cloudinary.com/py/image/upload/c_scale,h_721/v1503345048/bigstock-business-people-cooperation-86079140_semmbc.jpg);">
                            <div class="mbr-table-cell">
                                <div class="mbr-overlay"></div>
                                <div class="container-slide container">

                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 text-xs-center">
                                            <h1 class="mbr-section-title display">WE ARE STRONGER TOGETHER</h1>
                                            <p class="mbr-section-lead lead"><strong>Join A Team Of People On This Home Base Earning Platform.
                                                    Everyone Contributes To The Progress Of The Other</strong>
                                            </p>
                                            <p class="mbr-section-lead lead">
                                                <strong> With A Linked-Up Team, You Are Never Left Behind.</strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(http://res.cloudinary.com/py/image/upload/c_scale,h_327/v1503345020/WAP_IMAGES_14_2_kruaz7.png);">
                            <div class="mbr-table-cell">
                                <div class="mbr-overlay"></div>
                                <div class="container-slide container">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 text-xs-center">
                                            <h2 class="mbr-section-title display">BE ON TOP OF THE WORLD WITH BITYCOM</h2>
                                            <p class="mbr-section-lead lead"><strong>With Our Simple 2*2 Recycling Matrix Structure.
                                                Coupled With Access To Multiple Free Accounts. Donations To Your Bitcoin Wallet becomes Unlimited.
                                                </strong>
                                            </p>

                                            <div class="mbr-section-btn"><a class="btn btn-lg btn-info" href="{{url('/register')}}">Join Us today</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(assets/images/rangerover-2000x1320-84.jpg);">
                            <div class="mbr-table-cell">
                                <div class="mbr-overlay"></div>
                                <div class="container-slide container">

                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 text-xs-center">
                                            <h2 class="mbr-section-title display-1">image</h2>
                                            <p class="mbr-section-lead lead">YOU CAN MAKE $100 DAILY, $700 WEEKLY, $3000 MONTHLY AND $36,500 YEARLY? <br><br>JUST FOR ONE (1) ACCOUNT! <br><br>HOW ABOUT ACCESS TO MULTIPLE ACCOUNTS FOR FREE? <br><br>IT ALL SEEMS IMPOSSIBLE? <br><br>STUDY OUR STRUCTURE VERY DEEPLY AND YOU WILL SEE ITS POSSIBILITY EVEN MORE <br><br>CLEARLY.<br></p>

                                            <div class="mbr-section-btn"><a class="btn btn-lg btn-success" href="{{url('/register')}}">Join Us</a>
                                                <a class="btn btn-lg btn-white btn-white-outline" href="{{url('/login')}}">Login</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    </div>

                    <a data-app-prevent-settings="" class="left carousel-control" role="button" data-slide="prev" href="#slider-o">
                        <span class="icon-prev" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a data-app-prevent-settings="" class="right carousel-control" role="button" data-slide="next" href="#slider-o">
                        <span class="icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-cards mbr-section mbr-section-nopadding" id="features7-4" style="background-color: rgb(239, 239, 239);">
        <div class="mbr-cards-row row">
            <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 40px; padding-bottom: 0px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img iconbox"><a href="https://#.com" class="etl-icon icon-phone mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></a></div>
                        <div class="card-block">
                            <h4 class="card-title">SIGN UP</h4>

                            <p class="card-text">Sign up with us to begin the greatest trip to a total financial freedom
                                <br>
                                <br>A place where income is easy and sustainable.</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 40px; padding-bottom: 0px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img iconbox"><span class="mbri-touch mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></span></div>
                        <div class="card-block">
                            <h4 class="card-title">ACTIVATE ACCOUNT</h4>

                            <p class="card-text">Activate your account by simply donating $25 bitcoin to your upline and your account is set to
                                <br>
                                <br>start receiving donations from your downlines anywhere in the world.</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 40px; padding-bottom: 0px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img iconbox"><span class="etl-icon icon-profile-male mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></span></div>
                        <div class="card-block">
                            <h4 class="card-title">INVITE FRIENDS</h4>

                            <p class="card-text">Build your empire by inviting just two (2) friends to form your team, as your friends also invite
                                <br>
                                <br>their friends your empire is built.</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 40px; padding-bottom: 0px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img iconbox"><span class="mbri-cash mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></span></div>
                        <div class="card-block">
                            <h4 class="card-title">EARN</h4>

                            <p class="card-text">Earn $100 daily, $700 weekly, $3000 monthly and $36,500 year.
                                <br>Unbreakable and sustainable earning platform.</p>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <section class="mbr-cards mbr-section mbr-section-nopadding mbr-parallax-background" id="features1-5" style="background-image: url(assets/images/jumbotron.jpg);">

        <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
        </div>

        <div class="mbr-cards-row row striped">

            <div class="mbr-cards-col col-xs-12 col-lg-6" style="padding-top: 40px; padding-bottom: 40px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img"><img src="assets/images/wap-image-8-600x205.jpg" class="card-img-top"></div>
                        <div class="card-block">
                            <h4 class="card-title">CONTROL YOUR TIME AND RESOURCES
                                <div><br></div></h4>

                            <p class="card-text"><br>Making money on our platform gives you a full controll of your time and resources
                                <br>
                                <p>
                                <br>Through our multiple account system you are sure to receive money from multiple streams.
                                <br>Bitcoin makes it easy to receive money from anywhere in the world in just minutes
                                <br>You are always with your cash anywhere in the world you find yourself.</p>

                        </div>
                        </div>
                    </div>
                </div>

            <div class="mbr-cards-col col-xs-12 col-lg-6" style="padding-top: 40px; padding-bottom: 40px;">
                <div class="container">
                    <div class="card cart-block">
                        <div class="card-img"><img src="assets/images/wap-image-7-600x360.jpg" class="card-img-top"></div>
                        <div class="card-block">
                            <h4 class="card-title">WITH US YOUR DREAM CARS, HOME AND TOURIST VISIT IS WITHIN YOUR REACH
                                <div><br></div></h4>

                            <p class="card-text">For how long have you dreamt of that suitable car, that cute building, tourist visit to Dubai,
                                <br>
                                <br>Paris, Africa and a visit to the historic London Bridge.
                                <br>
                                <br>Join us and generate income from all part of the world from the comfort of your home, city and country
                                <br>
                            </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section mbr-parallax-background" id="testimonials3-7" style="background-image: url(assets/images/landscape2.jpg); padding-top: 40px; padding-bottom: 0px;">

        <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
        </div>

        <div class="mbr-section mbr-section__container mbr-section__container--middle">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-xs-center">
                        {{--<h3 class="mbr-section-title display-2">WHAT OTHERS ARE SAYING</h3>--}}

                    </div>
                </div>
            </div>
        </div>


        {{--<div class="mbr-testimonials mbr-section mbr-section-nopadding">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}

                    {{--<div class="col-xs-12 col-lg-4">--}}

                        {{--<div class="mbr-testimonial card">--}}
                            {{--<div class="card-block"><p>WITH US YOUR DREAM CARS, HOME AND TOURIST VISIT IS WITHIN YOUR REACH--}}
                                {{--</p><p><br></p></div>--}}
                            {{--<div class="mbr-author card-footer">--}}

                                {{--<div class="mbr-author-name">Abanoub S.</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div><div class="col-xs-12 col-lg-4">--}}

                        {{--<div class="mbr-testimonial card">--}}
                            {{--<div class="card-block"><p>WITH US YOUR DREAM CARS, HOME AND TOURIST VISIT IS WITHIN YOUR REACH--}}
                                {{--</p><p><br></p></div>--}}
                            {{--<div class="mbr-author card-footer">--}}

                                {{--<div class="mbr-author-name">Suffian A.</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div><div class="col-xs-12 col-lg-4">--}}

                        {{--<div class="mbr-testimonial card">--}}
                            {{--<div class="card-block"><p>WITH US YOUR DREAM CARS, HOME AND TOURIST VISIT IS WITHIN YOUR REACH--}}
                                {{--</p><p><br></p></div>--}}
                            {{--<div class="mbr-author card-footer">--}}

                                {{--<div class="mbr-author-name">Jhollman C.</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}

        {{--</div>--}}

    </section>

    <section class="mbr-section mbr-section-md-padding" id="social-buttons4-8" style="background-color: rgb(46, 46, 46); padding-top: 10px; padding-bottom: 0px;">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-xs-center" id="btc">
                    <h6 class="mbr-section-title display-4">BUY AND SELL BITCOIN</h6>
                    <p style="color:white;">
                        We recommend these tested, trusted and verified
                        exchangers all over the world by BITYCOM  in
                        these listed countries, exchange bitcoin from the comfort
                        of your home and without fear with these listed exchangers.

                        Click on your country and exchange your bitcoin.

                    </p>
                    <p style="color:white;">
                        <b>NOTE: </b> Past performance is not and indicator of future
                        result.
                    </p>
                    <a href="{{route('pub-exchanges')}}" class="btn btn-primary">Buy or Sell BTC</a>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section mbr-section-md-padding" id="social-buttons4-8" style="background-color: rgb(46, 46, 46); padding-top: 10px; padding-bottom: 0px;">

        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-8 col-md-offset-2 text-xs-center">--}}
                    {{--<h6 class="mbr-section-title display-4">FOLLOW US</h6>--}}
                    {{--<div><a class="btn btn-social" title="Twitter" target="_blank" href="https://twitter.com/#"><i class="socicon socicon-twitter"></i></a>--}}
                        {{--<a class="btn btn-social" title="Facebook" target="_blank" href="">--}}
                            {{--<i class="socicon socicon-facebook"></i></a>         </div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>



@endsection
