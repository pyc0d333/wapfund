
@extends('layouts.auth-layout')

<?php

$admin = App\User::where('is_superadmin', true)->first();
$ref = $_REQUEST['ref'];
$sponsor = App\User::find($ref);

?>
@section('content')
    <div class="container-fluid" >
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle">
                <div class="auth-form  ml-auto mr-auto no-float" style="background-color: #081F2C">
                    <div class="panel panel-default card-view mb-0" >
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title text-center txt-dark">REGISTER</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    @if($sponsor != null)
                                        <p>Sponsor<strong class="label label-primary pull-right">{{$sponsor->name}}</strong></p>
                                        <p>Contact<strong class="label label-primary pull-right">{{$sponsor->email}} </strong></p>
                                    @else
                                        <p>Sponsor <strong class="label label-primary pull-right">{{$admin->name}}</strong>
                                        <p>Contact<strong class="label label-primary pull-right">{{$admin->email}}</strong></p></p>

                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-wrap">
                                            <form class="form-horizontal orm-validation mt-20" name="form" role="form" method="POST" action="{{ route('register') }}">
                                                {{ csrf_field() }}
                                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label for="name" class="col-md-3 control-label">Name</label>

                                                    <div class="col-md-9">
                                                        <input id="name" type="text" class="form-control underline-input" name="name" value="{{ old('name') }}" required autofocus>

                                                        @if ($errors->has('name'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>                                  <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                    <label for="username" class="col-md-3 control-label">Username</label>

                                                    <div class="col-md-9">
                                                        <input id="name" type="text" class="form-control underline-input" name="username" value="{{ old('username') }}" required autofocus>

                                                        @if ($errors->has('username'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="email" class="col-md-3 control-label">E-Mail</label>

                                                    <div class="col-md-9">
                                                        <input id="email" type="email" class="form-control underline-input" name="email" value="{{ old('email') }}" required>

                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label for="phone" class="col-md-3 control-label">Phone</label>

                                                    <div class="col-md-9">
                                                        <input id="phone"  class="form-control underline-input" name="phone" value="{{ old('phone') }}" required>

                                                        @if ($errors->has('phone'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>


                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label for="password" class="col-md-3 control-label">Password</label>

                                                    <div class="col-md-9">
                                                        <input id="password" type="password" class="form-control underline-input" name="password" required>

                                                        @if ($errors->has('password'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="password-confirm" class="col-md-3 control-label">Confirm Password</label>

                                                    <div class="col-md-9">
                                                        <input id="password-confirm" type="password" class="form-control underline-input" name="password_confirmation" required>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <br>
                                                    Got an account already?
                                                    <a href="{{route ('login')}}" class="text-lightred">Login Here</a>

                                                </div>

                                                <div class="form-group bg-slategray lt wrap-reset mt-20 text-left">
                                                    <div class="col-md-12 ">
                                                        <button type="submit" class="btn btn-danger btn-block">
                                                            Register
                                                        </button>
                                                    </div>
                                                </div>
                                                @if(isset($_GET['ref']))
                                                    <input type="hidden" name="referrer" value="{{ $_GET['ref'] }}">

                                                @else
                                                    <input type="hidden" name="referrer" value="1">
                                                @endif


                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection()