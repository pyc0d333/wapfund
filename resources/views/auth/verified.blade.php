@extends('layouts.dashboard')

@section('body')

    <div class="page page-core page-login">

        <div class="container w-420 p-15 bg-white mt-40 text-center">


            <h2 class="text-light text-greensea">Verified Acccount</h2>

            <hr class="b-3x">

            <div class="bg-slategray lt wrap-reset mt-40">
                <p class="m-0">
                    <a href="{{route('login')}}" class="text-uppercase"><strong class="label label-success">Proceed to Login</strong></a>
                </p>
            </div>

        </div>

    </div>
@endsection
