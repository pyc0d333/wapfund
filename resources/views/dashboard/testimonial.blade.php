@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection
@section('content')

<div class="row">
        <div class="col-md-6">
            <h3 class="text-center">Please Submit a Testimonial</h3>
            <form method="POST" action="{{ route('add_testimonial', Auth::user()->id) }}" name="form">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('testimony') ? ' has-error' : '' }} has-feedback">
                    <label>Enter testimonial</label>
                    <textarea type="text" class="form-control"  name="testimony" value="{{ old('testimony') }}" required></textarea>
            <span class="glyphicon glyphicon-ugser form-control-feedback"></span>
                        @if ($errors->has('testimony'))
                            <span class="help-block">
                <strong>{{ $errors->first('testimony') }}</strong>
            </span>
                    @endif
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        </div>

@endsection