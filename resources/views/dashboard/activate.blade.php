@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection

<?php

$sponsorId = Auth::user()->referrer;

if(Auth::user()->level == 0 && Auth::user()->phase == 0)
{
    $sponsor = App\User::find($sponsorId);

    if($sponsor->loan_account || $sponsor->level != 1 || !$sponsor->is_activated)
    {
        $sponsor = App\User::find(1);

    }

}

else
{
    $sponsor = App\User::find($sponsorId);
    $sponsorId = $sponsor->referrer;
    $sponsor = App\User::find($sponsorId);

    if($sponsor->loan_account || $sponsor->level != 2 || !$sponsor->is_activated)
    {
        $sponsor = App\User::find(1);

    }

}
?>


@section('content')
    <div class="row">
        <!-- col -->
        <!-- /col -->
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Activate Account<strong class="txt-primary"> {{Auth::user()->username}}</strong></h6>
                        @if($user->level != 1)
                            <div class="alert alert-danger">
                                Pay up $25 to activate your account
                            </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <span class="font-12 mb-5"><strong>Level</strong></span>
                                    <p class="txt-dark">{{Auth::user()->level}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <span class="font-12 mb-5"><strong>Sponsor Name</strong></span>
                                    <p  class="txt-dark">{{$sponsor->name}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-success">
                                <div class="sl-content">
                                    <span class="font-12 mb-5">Sponsor Email</span>
                                    <p  class="txt-dark">{{$sponsor->email}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-warning">
                                <div class="sl-content">
                                    <span class="font-12 mb-5">Sponsor Phone</span>
                                    <p  class="txt-dark">{{$sponsor->phone}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-warning">
                                <div class="sl-content">
                                    <span class="font-12 mb-5">Sponsor BTC Address</span>
                                    <p  class="txt-danger"><strong>{{$sponsor->btc_address}}</strong></p>
                                </div>
                            </div>
                            <form class="form-horizontal form-validation mt-20"  name="form"  role="form" method="POST" action="{{ route('new_transaction', [Auth::user()->id, $sponsor->id]) }}">

                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('tx_hash') ? ' has-error' : '' }}">
                                                <label for="wallet_id" class="col-md-3 control-label">Transaction Hash</label>
                                                <div class="col-md-9 col-sm-12">
                                                    <input id="wallet_id" type="text" class="form-control underline-input" name="tx_hash" value="{{ old('tx_hash') }}" required autofocus>

                                                    @if ($errors->has('tx_hash'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('tx_hash') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('amount_paid') ? ' has-error' : '' }}">
                                                <label for="amount" class="col-md-3 control-label">Amount Paid</label>

                                                <div class="col-md-9 col-sm-12">
                                                    <input id="amount" type="number" class=" underline-input form-control" name="amount_paid" required>

                                                    @if ($errors->has('amount_paid'))
                                                        <span class="help-block">
                            <strong>{{ $errors->first('amount_paid') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            @if(!$user->awaiting_payment_confirmation)
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" class="btn btn-rounded btn-primary btn-block btn-sm">Activate Account</button>
                                                    </div>
                                                </div>
                                            @else

                                                <p class="label label-warning">You have already requested payment confirmation</p>
                                            @endif

                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /col -->
    </div>

@endsection