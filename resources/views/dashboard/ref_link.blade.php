@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection

<?php

        $loanCode = mt_rand(50000, 302023);

?>

@section('content')
    <!-- row -->
    <div class="row">

        <div class="col-md-4">
            <div class="panel panel-primary card-view">
                <div class="panel-heading mb-20">
                    <div class="pull-left">
                        <h6 class="panel-title txt-light pull-left">Referral Links</h6>
                    </div>
                    <div class="pull-right">
                        <a class="txt-light" href="javascript:void(0);"><i class="ti-plus"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if(Auth::user()->is_activated == true  || Auth::user()->loan_account)

                        <ul class="chat-list-wrap">
                                <li class="chat-list">
                                    <div class="chat-body">
                                        <div class="chat-data">
                                            <div class="user-data">
                                                <span class="name block capitalize-font">Referral Link</span>
                                                <span class="timck "><code>
                            {{  url('register') . '?ref=' . Auth::user()->id}}
                        </code>
</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="chat-list">
                                    <div class="chat-body">
                                        <div class="chat-data">
                                            <div class="user-data">
                                                <span class="name block capitalize-font">Loan Account Link</span>
                                                <span class="timck ">     <code>
                            {{  url('register') . '?loan=' . $loanCode }}
                        </code>
</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </li>

                        </ul>
                        @else
                            <p class="alert alert-danger">Please pay to activate your account before you can get a ref link</p>
                            <a href="{{route('activate_account')}}" class="btn btn-primary  btn-flat">Pay to Activate your account please</a>
@endif
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection