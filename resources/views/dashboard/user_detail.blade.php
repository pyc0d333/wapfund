@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Bitycom') }}</title>
@endsection


@section('content')
    <div class="page page-profile">


    <?php
    $sponsorId =  $user->referrer;
    $sponsor = App\User::find($sponsorId);

    ?>

    <!-- page content -->
        <div class="pagecontent">



            <!-- row -->
            <div class="row">

            @include('dashboard.notifications')


            <!-- col -->
                <div class="col-md-5">

                    @if(Auth::user()->btc_address == '')
                        <h3 class="text-center alert alert-danger">Add Bitcoin Address to receive payments</h3>
                        <form method="POST" action="{{ route('add_btc_address', Auth::user()->id) }}" name="form">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('btc_address') ? ' has-error' : '' }} has-feedback">
                                <label>BTC ADDRESS</label>
                                <input type="text" class="form-control" placeholder="{{ Auth::user()->btc_address }}" name="btc_address" value="{{ old('btc_address') }}" required>
                                <span class="glyphicon glyphicon-ugser form-control-feedback"></span>
                                @if ($errors->has('btc_address'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('btc_address') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="row">

                                <div class="col-md-8">

                                </div>
                                <!-- /.col -->
                                <div class="col-xs-4">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                @else
                @endif

                <!-- tile -->
                    <section class="tile tile-simple">

                        <!-- tile widget -->
                        <div class="tile-widget p-30 text-center">


                            <div class="media-body">
                                <ul class="list">
                                    <li class="list-group-item"><span class=""><strong>{{$me->name}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->email}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->phone}}</strong></span></li>
                                    {{--<li class="list-group-item"><span class=""><strong>{{$me->btc_address}}</strong></span></li>--}}
                                    <li class="list-group-item"><span class=""><strong>Level {{$me->level}}</strong></span></li>
                                    @if(!$me->loan_account)
                                        <li class="list-group-item"><span class=""><strong class="label label-success">Standard Account</strong></span></li>
                                    @else
                                        <li class="list-group-item"><span class=""><strong class="label label-danger">Loan Account</strong></span></li>
                                    @endif
                                </ul>

                            </div>
                            <hr>
                            @if($me->is_superadmin == false)
                                <div>
                                    <h4 class="center-text">Sponsor Information</h4>
                                    <ul class="list-group">
                                        <li class="list-group-item">Name<span class="pull-right label label-primary"><strong>{{$sponsor->name}}</strong></span></li>
                                        <li class="list-group-item">Email<span class="pull-right label label-primary"><strong>{{$sponsor->email}}</strong></span></li>
                                        <li class="list-group-item">Phone<span class="pull-right label label-primary"><strong>{{$sponsor->phone}}</strong></span></li>
                                        {{--@if(Auth::user()->loan_account)--}}
                                            {{--<li class="list-group-item">BTC ADDRESS<span class="pull-right label label-danger"><strong>{{$sponsor->btc_address}}</strong></span></li>--}}
                                        {{--@else--}}

                                        {{--@endif--}}
                                    </ul>

                                </div>

                            @else
                            @endif

                        </div>

                        <!-- /tile widget -->

                    </section>

                </div>

                <div class="row">

                    <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Recycles</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="sm-graph-box">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="counter-wrap text-right">
                                                    <span class="counter-cap"></span><span class="counter">{{Auth::user()->recycle_count}}</span><span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark"><strong>Payments</strong></h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="sm-graph-box">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="counter-wrap text-right">
                                                    <span class="counter-cap"></span><span class="counter">${{Auth::user()->help_count * 25}}</span><span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark"><strong>Earnings</strong></h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="sm-graph-box">
                                        <div class="row">

                                            <div class="col-xs-6">
                                                <div class="counter-wrap text-right">
                                                    <span class="counter-cap"></span><span class="counter">${{Auth::user()->received_count * 25}}</span><span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark"><strong>Level</strong></h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="sm-graph-box">
                                        <div class="row">

                                            <div class="col-xs-6">
                                                <div class="counter-wrap text-right">
                                                    <span class="counter-cap"></span><span class="counter">{{Auth::user()->level}}</span><span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        <div class="row">
            <div class="col sm-12 col-md-6 col-md-offset-3">

                <div class="media">
                    <div class="media-body">

                        <ul class="list-group" style="border: 3px solid  blue; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-primary"><strong>{{$user->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-primary"><strong>{{$user->email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-primary"><strong>{{$user->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col sm-12 col-md-4 col-md-offset-2">

                <div class="media ">
                    <div class="media-body">

                        <ul class="list-group" style="border: 3px solid  dimgrey; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-warning"><strong>{{$left->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-warning"><strong>{{$left->email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-warning"><strong>{{$left->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col sm-12 col-md-4 " >

                <div class="media" >
                    <div class="media-body" >

                        <ul class="list-group" style="border: 3px solid red; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-danger"><strong>{{$right->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-danger"><strong>{{$right->email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-danger"><strong>{{$right->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">
                        <ul class="list-group"  style="border: 3px solid  dimgrey; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-warning"><strong>{{$leftDownleft->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-warning"><strong>{{$leftDownleft->email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-warning"><strong>{{$leftDownleft->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div> <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">

                        <ul class="list-group"  style="border: 3px solid dimgrey; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-warning"><strong>{{$leftDownright->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-warning"><strong>{{$leftDownright>email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-warning"><strong>{{$leftDownright->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div> <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">


                        <ul class="list-group" style="border: 3px solid red; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-danger"><strong>{{$rightDownleft->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-danger"><strong>{{$rightDownleft->email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-danger"><strong>{{$rightDownleft->phone}}</strong></span></li>
                        </ul>


                    </div>
                </div>
            </div> <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">


                        <ul class="list-group" style="border: 3px solid red; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"> Name<span class="pull-right label label-danger"><strong>{{$rightDownright->name}}</strong></span></li>
                            <li class="list-group-item"> Email<span class="pull-right label label-danger"><strong>{{$rightDownright->email}}</strong></span></li>
                            <li class="list-group-item"> Phone<span class="pull-right label label-danger"><strong>{{$rightDownright->phone}}</strong></span></li>
                        </ul>


                    </div>
                </div>
            </div>
        </div>
            <!-- row -->
            <div class="row">

                <!-- col -->
                <div class="col-sm-12 portlets sortable">

                @include('dashboard.notifications')

                <!-- tile -->
                    <section class="tile">

                        <!-- tile header -->
                        <div class="tile-header">
                            <h1 class="custom-font"><strong>All</strong>Logs</h1>
                            <ul class="controls">
                                <li class="dropdown">

                                    <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                        <i class="fa fa-spinner fa-spin"></i>
                                    </a>

                                    <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                        <li>
                                            <a role="button" tabindex="0" class="tile-toggle">
                                                <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                                <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a role="button" tabindex="0" class="tile-refresh">
                                                <i class="fa fa-refresh"></i> Refresh
                                            </a>
                                        </li>
                                        <li>
                                            <a role="button" tabindex="0" class="tile-fullscreen">
                                                <i class="fa fa-expand"></i> Fullscreen
                                            </a>
                                        </li>
                                    </ul>

                                </li>
                            </ul>
                        </div>
                        <!-- /tile header -->

                        <!-- tile body -->
                        <div class="tile-body p-0">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Activity</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td><?php
                                            $user = App\User::where('id', $log->user_id)->first();
                                            ?>
                                            <a href="{{route('user-detail', $user->id)}}" target="_blank">{{$user->username}}</a>
                                        </td>
                                        <td>{{$log->action}}</td>
                                        <td>{{$log->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{ $logs->links() }}


                        </div>
                        <!-- /tile body -->

                    </section>
                    <!-- /tile -->
                </div>
            </div>


@endsection