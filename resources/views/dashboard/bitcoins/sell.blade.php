@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')

    @include('dashboard.notifications')
    <div class="row">
        <!-- col -->
        <div class="col-md-6">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h2 class="text-light text-greensea">SELL YOUR BTC </h2>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <h4 class="text-center">Place Your Selling Order Here</strong> </h4>
                    <div>
                    </div>
                    <form class="form-horizontal form-validation mt-20"  name="form"  role="form" method="POST" action="{{route ('bitcoin_order', Auth::user()->id)}}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('order_amount') ? ' has-error' : '' }}">
                            <label for="order_amount" class="col-md-3 control-label">Amount</label>

                            <div class="col-md-9">
                                <input id="order_amount" type="number" class="form-control underline-input" name="order_amount" value="{{ old('order_amount') }}" required autofocus>

                                @if ($errors->has('order_amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('order_amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('payment_method') ? ' has-error' : '' }}">
                            <label for="method" class="col-md-3 control-label">Payment Method</label>
                            <div class="col-md-9">
                                <input id="method"  class=" underline-input form-control" name="payment_method" required>
                                @if ($errors->has('payment_method'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('payment_method') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('trans_id') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-3 control-label"></label>

                            <div class="col-md-9">
                                <input id="amount" type="text" class=" underline-input form-control" name="trans_id" required>

                                @if ($errors->has('trans_id'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('trans_id') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-rounded btn-slategray btn-block btn-sm">Submit Order</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

            <hr>

        </div>
    </div>
    <div class="row">
        <!-- col -->
        <div class="col-sm-12 portlets sortable">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header">
                    <h1 class="custom-font"><strong>My Bitcoin Buy Orders</strong></h1>
                    <ul class="controls">
                        <li class="dropdown">

                            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                                <i class="fa fa-spinner fa-spin"></i>
                            </a>

                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                <li>
                                    <a role="button" tabindex="0" class="tile-toggle">
                                        <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                        <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-refresh">
                                        <i class="fa fa-refresh"></i> Refresh
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-fullscreen">
                                        <i class="fa fa-expand"></i> Fullscreen
                                    </a>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body p-0">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Order Amount</th>
                            <th>Amount Paid</th>
                            <th>Transaction Id</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)

                            <tr>
                                <td>{{$order->order_amount}}</td>
                                <td>{{$order->amount_paid}}</td>
                                <td>{{$order->trans_id}}</td>
                                <td>{{$order->status}}</td>
                                <td>{{$order->created_at}}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection