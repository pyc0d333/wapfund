@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection

<?php



$sponsorId = Auth::user()->referrer;

if(Auth::user()->level == 1)
{
    $sponsor = App\User::find($sponsorId);

}

if(Auth::user()->level == 0 && Auth::user()->phase == 0)
{
    $sponsor = App\User::find($sponsorId);

}

else
{
    $sponsor = App\User::find($sponsorId);
    $sponsorId = $sponsor->referrer;
    $sponsor = App\User::find($sponsorId);

}
?>
@section('content')
    <br>


    <!-- /Row -->
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="panel panel-primary card-view">
                <div class="panel-heading mb-20">
                    <div class="pull-left">
                        <h6 class="panel-title txt-light pull-left">My Accounts</h6>
                    </div>
                    <div class="pull-right">
                        <a class="txt-light" href="javascript:void(0);"><i class="ti-plus"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <ul class="chat-list-wrap">
                            @foreach($accounts as $account)

                                <li class="chat-list">
                                    <div class="chat-body">
                                        <div class="chat-data">
                                            <img class="user-img img-circle"  src="dist/img/user.png"  alt="user"/>

                                            <div class="user-data">
                                                <span class="name block capitalize-font">{{$account->username}}</span>
                                                <span class="timck "> <a class=" time block txt-danger" href="{{route('inner-upgrade', $account->id)}}">Upgrade this account</a></span>
                                                <span class="timck "> <a class=" time block txt-primary btn btn-danger btn-small" href="{{route('user-detail', $account->id)}}"  target="_blank">View Account Information</a></span>
                                            </div>
                                            <div class="status away"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Recycles</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">{{Auth::user()->recycle_count}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"><strong>Payments</strong></h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">${{Auth::user()->help_count * 25}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"><strong>Earnings</strong></h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">

                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">${{Auth::user()->received_count * 25}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><strong>Level</strong></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="sm-graph-box">
                                <div class="row">

                                    <div class="col-xs-6">
                                        <div class="counter-wrap text-right">
                                            <span class="counter-cap"></span><span class="counter">{{Auth::user()->level}}</span><span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><strong>Status</strong></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="sm-graph-box">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="counter-wrap text-left">
                                        <span class="counter"> @if(!Auth::user()->loan_account)
                                                <strong class="label label-success">Standard Account</strong>
                                            @else
                                                <strong class="label label-danger">Loan Account</strong></span>
                                            @endif</span>
                                            <span>

                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection