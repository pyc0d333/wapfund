@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')
    <div class="page page-profile">


    <?php
    $referrer = Auth::user()->referrer;
    $referrer = App\User::find($referrer);


    ?>

    <!-- page content -->
        <div class="pagecontent">

            <!-- row -->
            <div class="row">

                <!-- col -->

                <div class="col-md-9">
                    <h1 class="text-center alert alert-danger">Your Last payment is still awaiting confirmation, Please hang on for a little while</h1>
                </div>

                <!-- /row -->
            </div>
            <!-- /page content -->

        </div>

@endsection