@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')
    <div class="page page-profile">


<?php
    $user_id = Auth::user()->id;
        $me = App\User::find($user_id);
        $sponsorId =  Auth::user()->referrer;
        $sponsor = App\User::find($sponsorId);
        ?>

        <!-- page content -->
        <div class="pagecontent">
            <br>


            <!-- row -->
            <div class="row">

                @include('dashboard.notifications')
                <div class="col-lg-4 col-md-3 col-sm-12">
                    <div class="panel panel-primary card-view">
                        <div class="panel-heading mb-20">
                            <div class="pull-left">
                                <h6 class="panel-title txt-light pull-left">My Profile</h6>
                            </div>
                            <div class="pull-right">
                                <a class="txt-light" href="javascript:void(0);"><i class="ti-plus"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="col-md">

                                    @if(Auth::user()->btc_address == '')
                                        <h3 class="text-center alert alert-danger">Add Bitcoin Address to receive payments</h3>
                                        <form method="POST" action="{{ route('add_btc_address', Auth::user()->id) }}" name="form">
                                            {{ csrf_field() }}
                                            <div class="form-group{{ $errors->has('btc_address') ? ' has-error' : '' }} has-feedback">
                                                <label>BTC ADDRESS</label>
                                                <input type="text" class="form-control" placeholder="{{ Auth::user()->btc_address }}" name="btc_address" value="{{ old('btc_address') }}" required>
                                                <span class="glyphicon glyphicon-ugser form-control-feedback"></span>
                                                @if ($errors->has('btc_address'))
                                                    <span class="help-block">
                                    <strong>{{ $errors->first('btc_address') }}</strong>
                                </span>
                                                @endif
                                            </div>

                                            <div class="row">

                                                <div class="col-md-8">

                                                </div>
                                                <!-- /.col -->
                                                <div class="col-xs-4">
                                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                        </form>
                                    @else
                                    @endif
                                </div>
                                <ul class="list">
                                    <li class="list-group-item"><span class=""><strong>{{$me->name}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->email}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->phone}}</strong></span></li>
                                    {{--<li class="list-group-item"><span class=""><strong>{{$me->btc_address}}</strong></span></li>--}}
                                    {{--<li class="list-group-item"><span class="pull-left"><strong>Recycles</strong></span><span class="red-text">{{$me->recycle_count}}</span></li>--}}
                                    <li class="list-group-item"><span class=""><strong>Level {{$me->level}}</strong></span></li>
                                    @if(!$me->loan_account)
                                        <li class="list-group-item"><span class=""><strong class="label label-success">Standard Account</strong></span></li>
                                    @else
                                        <li class="list-group-item"><span class=""><strong class="label label-danger">Loan Account</strong></span></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-12">
                    <div class="panel panel-primary card-view">
                        <div class="panel-heading mb-20">
                            <div class="pull-left">
                                <h6 class="panel-title txt-light pull-left">Sponsor Info</h6>
                            </div>
                            <div class="pull-right">
                                <a class="txt-light" href="javascript:void(0);"><i class="ti-plus"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                 <ul class="list">
                                    <li class="list-group-item"><span class=""><strong>{{$sponsor->name}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$sponsor->email}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$sponsor->phone}}</strong></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                    <h6 class="text-center ">Request Change of Bitcoin Address</h6>
                    <p class="alert alert-danger">Due to our security policies, you need to submit a request to update your bitcoin address
                        <br>Tell us why you want to update your account and we will get back to you on the phone number you signed up with</p>

                    <form method="POST" action="{{ route('new_ticket', Auth::user()->id) }}" name="form">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }} has-feedback">
                            <label>Message</label>
                            <textarea type="text" class="form-control" placeholder="Type Your Message Here" name="message" value="{{ old('message') }}" required></textarea>
                            <span class="glyphicon glyphicon-ugser form-control-feedback"></span>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-3 control-label">Phone</label>

                                <input id="phone" type="number" class="form-control " name="phone" value="{{ old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                             </span>
                                @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
            <!-- /row -->
        </div>
        <!-- /page content -->

    </div>

@endsection