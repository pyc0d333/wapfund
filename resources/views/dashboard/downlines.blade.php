@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')

@if($user->id != 1)
    <br>
    <div class="row">
        <div class="col sm-12 col-md-6 col-md-offset-3">

            <div class="media">
                <div class="media-body">

                    <ul class="list-group" style="border: 3px solid  blue; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$user->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email<span class="pull-right"><strong>{{$user->email}}</strong></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$user->phone}}</strong></span></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col sm-12 col-md-4 col-md-offset-2">

            <div class="media ">
                <div class="media-body">
                    <ul class="list-group" style="border: 3px solid  orange; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$left->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email</span><span class="pull-right"><strong>{{$left->email}}</strong></span></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$left->phone}}</strong></span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col sm-12 col-md-4" >

            <div class="media" >
                <div class="media-body" >
                    <ul class="list-group" style="border: 3px solid  orange; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$left->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email</span><span class="pull-right"><strong>{{$left->email}}</strong></span></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$left->phone}}</strong></span></li>
                    </ul>

                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-12 col-md-3">
            <div class="media">
                <div class="media-body">
                    <ul class="list-group" style="border: 3px solid  greenyellow; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$left->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email</span><span class="pull-right"><strong>{{$left->email}}</strong></span></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$left->phone}}</strong></span></li>
                    </ul>

                </div>
            </div>
        </div> <div class="col-sm-12 col-md-3">
            <div class="media">
                <div class="media-body">
                    <ul class="list-group" style="border: 3px solid  greenyellow; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$left->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email</span><span class="pull-right"><strong>{{$left->email}}</strong></span></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$left->phone}}</strong></span></li>
                    </ul>
                </div>
            </div>
        </div> <div class="col-sm-12 col-md-3">
            <div class="media">
                <div class="media-body">

                    <ul class="list-group" style="border: 3px solid  greenyellow; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$left->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email</span><span class="pull-right"><strong>{{$left->email}}</strong></span></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$left->phone}}</strong></span></li>
                    </ul>
                </div>
            </div>
        </div> <div class="col-sm-12 col-md-3">
            <div class="media">
                <div class="media-body">
                    <ul class="list-group" style="border: 3px solid  greenyellow; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        <li class="list-group-item"><span class="">Name</span><span class="pull-right"><strong>{{$left->username}}</strong></span></li>
                        <li class="list-group-item"><span class="">Email</span><span class="pull-right"><strong>{{$left->email}}</strong></span></li>
                        <li class="list-group-item"><span class="">Phone</span><span class="pull-right"><strong>{{$left->phone}}</strong></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@elseif($user->id == 1)
    <div class="row">

        <div class="col-md-6">
            <section class="tile tile-simple">
                <div class="tile-header">
                    <h1 class="custom-font"><strong>Downlines</strong> List</h1>
                </div>
                <!-- /tile header -->
                <!-- tile body -->
                <div class="tile-body">
                 @forelse($admin_downlines as  $downline)
                    <div class="media">
                        <div class="media-body">

                            <ul class="list-group">
                                <li class="list-group-item"> Name<span class="pull-right label label-primary"><strong>{{$downline->name}}</strong></span></li>
                                <li class="list-group-item"> Username<span class="pull-right label label-primary"><strong>{{$downline->username}}</strong></span></li>
                                <li class="list-group-item"> Email<span class="pull-right label label-primary"><strong>{{$downline->email}}</strong></span></li>
                                <li class="list-group-item"> Phone<span class="pull-right label label-primary"><strong>{{$downline->phone}}</strong></span></li>
                            </ul>

                        </div>
                    </div>

                    <hr/>
                     @empty
                    <p>You have no downlines yet</p>

                     @endforelse

                </div>
                <!-- /tile body -->

            </section>

        </div>

               {{--<div class="col-md-6">--}}
            {{--<section class="tile tile-simple">--}}

                {{--<!-- tile header -->--}}
                {{--<div class="tile-header">--}}
                    {{--<h1 class="custom-font"><strong>Downlines</strong> List</h1>--}}
                {{--</div>--}}
                {{--<!-- /tile header -->--}}
                {{--<!-- tile body -->--}}
                {{--<div class="tile-body">--}}
                 {{--@forelse($tests as $t)--}}
                    {{--<div class="media">--}}
                        {{--<div class="media-body">--}}
                            {{--<p class="media-heading mb-0 mt-10">{{$t->name}}</p>--}}
                            {{--<small class="text-lightred"><strong>Level : </strong>{{$t->level}}</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr/>--}}
                     {{--@empty--}}
                    {{--<p>You have no downlines yet</p>--}}

                     {{--@endforelse--}}

                {{--</div>--}}
                {{--<!-- /tile body -->--}}

            {{--</section>--}}

        {{--</div>--}}
    </div>
@endif

@endsection