@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')
    <div class="page page-profile">


    <?php



    $sponsorId =  $user->referrer;
    $sponsor = App\User::find($sponsorId);


    ?>

    <!-- page content -->
        <div class="pagecontent">



            <!-- row -->
            <div class="row">

            @include('dashboard.notifications')


            <!-- col -->
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-success card-view">
                        <div class="panel-heading mb-20">
                            <div class="pull-left">
                                <h6 class="panel-title txt-light pull-left">My Accounts</h6>
                            </div>
                            <div class="pull-right">
                                <a class="txt-light" href="javascript:void(0);"><i class="ti-plus"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                                                           <div class="row">

                                                <div class="col-md-8">

                                                </div>
                                                <!-- /.col -->
                                                <div class="col-xs-4">
                                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                        </form>
                                    @else
                                    @endif
                                </div>
                                <ul class="list">
                                    <li class="list-group-item"><span class=""><strong>{{$me->username}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->name}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->email}}</strong></span></li>
                                    <li class="list-group-item"><span class=""><strong>{{$me->phone}}</strong></span></li>
                                    {{--<li class="list-group-item"><span class=""><strong>{{$me->btc_address}}</strong></span></li>--}}
                                    <li class="list-group-item"><span class=""><strong>Level {{$me->level}}</strong></span></li>
                                    @if(!$me->loan_account)
                                        <li class="list-group-item"><span class=""><strong class="label label-success">Standard Account</strong></span></li>
                                    @else
                                        <li class="list-group-item"><span class=""><strong class="label label-danger">Loan Account</strong></span></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">User Recycles</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">{{Auth::user()->recycle_count}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"><strong> User</strong> Payments</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">${{Auth::user()->help_count * 25}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"><strong> User </strong> Earnings</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">

                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">${{Auth::user()->received_count * 25}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"><strong> User</strong> Level</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">

                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-right">
                                                <span class="counter-cap"></span><span class="counter">{{Auth::user()->level}}</span><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark"><strong>Status</strong></h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="sm-graph-box">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="counter-wrap text-left">
                                        <span class="counter"> @if(!Auth::user()->loan_account)
                                                <strong class="label label-success">Standard Account</strong>
                                            @else
                                                <strong class="label label-danger">Loan Account</strong></span>
                                                @endif</span>
                                                <span>

                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="row">
            <div class="col sm-12 col-md-6 col-md-offset-2">

                <div class="media">
                    <div class="media-body">

                        <ul class="list-group" style="border: 3px solid  blue; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span class=""><strong>{{$user->name}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$user->email}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$user->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col sm-12 col-md-4 col-md-offset-1">

                <div class="media ">
                    <div class="media-body">

                        <ul class="list-group" style="border: 3px solid  dimgrey; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span><strong>{{$left->name}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$left->email}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$left->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col sm-12 col-md-4 " >

                <div class="media" >
                    <div class="media-body" >

                        <ul class="list-group" style="border: 3px solid red; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span><strong>{{$right->name}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$right->email}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$right->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">
                        <ul class="list-group"  style="border: 3px solid  dimgrey; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span class=""><strong>{{$leftDownleft->name}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$leftDownleft->email}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$leftDownleft->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div> <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">

                        <ul class="list-group"  style="border: 3px solid dimgrey; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span><strong>{{$leftDownright->name}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$leftDownright->email}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$leftDownright->phone}}</strong></span></li>
                        </ul>

                    </div>
                </div>
            </div> <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">


                        <ul class="list-group" style="border: 3px solid red; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span><strong>{{$rightDownleft->name}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$rightDownleft->email}}</strong></span></li>
                            <li class="list-group-item"><span><strong>{{$rightDownleft->phone}}</strong></span></li>
                        </ul>


                    </div>
                </div>
            </div> <div class="col-sm-12 col-md-3">
                <div class="media">
                    <div class="media-body">


                        <ul class="list-group" style="border: 3px solid red; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                            <li class="list-group-item"><span class=""><strong>{{$rightDownright->name}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$rightDownright->email}}</strong></span></li>
                            <li class="list-group-item"><span class=""><strong>{{$rightDownright->phone}}</strong></span></li>
                        </ul>


                    </div>
                </div>
            </div>
        </div>
            <!-- row -->
            <div class="row">

                <!-- col -->
                <div class="col-sm-10">

                @include('dashboard.notifications')

                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h4 class="panel-title txt-danger">User Logs</h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table class="table display product-overview mb-30" id="statement">
                                            <thead>
                                            <tr>
                                                <th>Activity</th>
                                                <th>Time</th>
                                            </tr>
                                            </thead>
                                <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td>{{$log->action}}</td>
                                        <td>{{$log->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{ $logs->links() }}


                        </div>
                        <!-- /tile body -->

                    </section>
                    <!-- /tile -->
                </div>
            </div>


@endsection