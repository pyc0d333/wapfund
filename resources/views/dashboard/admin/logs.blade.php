@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection

@section('content')
    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-sm-10">

        @include('dashboard.notifications')
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h4 class="panel-title txt-danger">Unactivated Accounts</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table display product-overview mb-30" id="statement">
                                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Activity</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($logs as $log)
                            <tr>
                                <td><?php
                                      $user = App\User::where('id', $log->user_id)->first();
                                      ?>
                                    <a href="{{route('user-detail', $user->id)}}" class="txt-primary" target="_blank">{{$user->username}}</a>
                                </td>
                                <td>{{$log->action}}</td>
                                <td>{{$log->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $logs->links() }}


                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection