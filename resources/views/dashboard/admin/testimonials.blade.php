@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection

@section('content')
    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-sm-10">

        @include('dashboard.notifications')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h4 class="panel-title txt-danger">Testimonials</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table display product-overview mb-30" id="statement">
                                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Testimony</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($testimonials as $testimonial)
                            <tr>
                                <td><?php
                                    $user = App\User::where('id', $testimonial->user_id)->first();
                                    ?>
                                    <a href="{{route('user-detail', $user->id)}}" target="_blank">{{$user->username}}</a>
                                </td>
                                <td>{{$testimonial->testimony}}</td>
                                <td>{{$testimonial->created_at->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $testimonials->links() }}

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection