@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection

@section('content')
    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-sm-12">

            @include('dashboard.notifications')

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h4 class="panel-title txt-danger">All Users</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table display product-overview mb-30" id="statement">
                                    <thead>
                          <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Suspend</th>
                            <th>Restore</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @if($user->suspended)
                                        <span class="label label-danger">suspended</span>
                                    @else
                                        <span class="label label-success">active</span>

                                    @endif
                                </td>
                                <td><a href="{{route('user-detail', $user->id)}}"><i class="fa fa-eye txt-success"></i></a></td>
                                <td>
                                @if($user->suspended)
                                    <a href="{{route('suspend_account', $user->id)}}" disabled=""><i class="fa fa-chevron-down txt-danger"></i></a>
                                    @else
                                    <a href="{{route('suspend_account', $user->id)}}"><i class="fa fa-chevron-down txt-danger"></i></a>
                                    @endif
                                </td>
                                <td>@if($user->suspended)
                                    <a href="{{route('restore_account', $user->id)}}" ><i class="fa fa-check-circle txt-success"></i></a>

                                    @else<a href="{{route('restore_account', $user->id)}}" disabled=""><i class="fa fa-check-circle txt-success"></i></a>

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection