@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-sm-10">

            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h4 class="panel-title txt-danger">Tickets</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table display product-overview mb-30" id="statement">
                                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>User</th>
                            <th>Message</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($all as $ticket)
                             <tr>
                                <td>{{$ticket->id}}</td>
                                <td>
                                    <?php
                                    $user = App\User::find($ticket->user_id);
                                    ?>
                                    {{$user->name}}</td>
                                <td>{{$ticket->message}}</td>
                                <td>{{$ticket->phone}}</td>
                                 @if($ticket->resolved == 1)
                                <td><span class="label label-success">resolved</span></td>
                                 @else
                                <td><span class="label label-danger">pending</span></td>
                                 @endif
                                 @if($ticket->resolved == 0)
                                 <td>
                                    <form  method="post" action="{{route('resolve_ticket', $ticket->id)}}">
                                        {{ csrf_field() }}
                                        <button class="btn btn-info" type="submit">
                                            Resolved
                                        </button>
                                    </form>
                                </td>
                                     @else
                                 <td></td>
                                     @endif
                            </tr>
                            @endforeach
                         </tbody>
                    </table>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection