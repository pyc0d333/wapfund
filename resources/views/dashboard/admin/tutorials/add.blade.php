@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name') }}</title>
@endsection

@section('content')

    <div class="row container">
        <br>

        <div class="col-md-8" >
            <form method="POST" action="{{route('save-tut')}}" name="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" class="form-input" required>
               </div>
                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }} has-feedback">
                    <label for="summer">Content</label>
                    <textarea type="text" class="form-control"  name="content" value="{{ old('content') }}"  id="summer" required></textarea>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('content'))
                        <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
                    @endif
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </form>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#summer').summernote({
                height: 200,                 // set editor heig// set maximum height of editor
                focus: true                  // set focus to editable area after initializin            });
            });
            $('#summer2').summernote({
                height: 200,                 // set editor heig// set maximum height of editor
                focus: true                  // set focus to editable area after initializin            });
            });
        });
    </script>

@endsection