@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name') }}</title>
@endsection

@section('content')

    <div class="row container">

        <div class="col-md-6">
            @include('flash::message')
            <a href="{{route('add_tut')}}" class="btn btn-primary">New Tutorial</a>
            <ul>
                @foreach($tuts as $tut)
                    <li><a href="{{route('edit-tut', $tut->id)}}">{{$tut->title}}</a>
                    </li>
                @endforeach
            </ul>
        </div>


    </div>

@endsection