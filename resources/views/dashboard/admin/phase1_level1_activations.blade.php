@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-sm-10">
            <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h4 class="panel-title txt-danger">Level 2 Activations</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table display product-overview mb-30" id="statement">
                                        <thead>
                        <tr>
                            <th>Sender</th>
                            <th>Sender Address</th>
                            <th>Receiver Address</th>
                            <th>Transaction Hash</th>
                            <th>Activate</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(Auth::user()->is_superadmin)
                            @foreach($users as $user)
                                <tr>
                                    <td><a href="{{route('user-detail', $user->id)}}" class="txt-primary" target="_blank">{{$user->username}}</a></td>
                                    <td>{{$user->btc_address}}</td>
                                    <td><?php
                                        $receiver = App\User::where('id', $user->current_sponsor)->first();
                                        ?>
                                        {{$receiver->btc_address}}
                                    </td>
                                    <td>
                                        <?php
                                        $tx_hash = App\Transaction::where('user_id', $user->id)->where('level', 0)->orderBy('created_at', 'DESC')->first();
                                        $tx_hash = $tx_hash->tx_hash;
                                        ?>
                                        <a href="https://blockchain.info/tx/{{$tx_hash}}" class="txt-primary" target="_blank">View Transaction</a></td>
                                    <td><form  method="post" action="{{route('phase_1_level_2', $user->id)}}">
                                            {{ csrf_field() }}
                                            <button class="btn btn-success" type="submit">
                                                Upgrade
                                            </button></form></td>
                                </tr>
                            @endforeach
                        @else
                            @foreach($nosers as $user)
                                <tr>
                                    <td><a href="{{route('user-detail', $user->id)}}" target="_blank">{{$user->username}}</a></td>
                                    <td>{{$user->btc_address}}</td>
                                    <td><?php
                                        $receiver = App\User::where('id', $user->current_sponsor)->first();
                                        ?>
                                        {{$receiver->btc_address}}
                                    </td>
                                    <td>
                                        <?php
                                        $tx_hash = App\Transaction::where('user_id', $user->id)->where('level', 0)->orderBy('created_at', 'DESC')->first();
                                        $tx_hash = $tx_hash->tx_hash;
                                        ?>
                                        <a href="https://blockchain.info/tx/{{$tx_hash}}" target="_blank">View Transaction</a></td>

                                    </td>
                                    <td><form  method="post" action="{{route('phase_1_level_2', $user->id)}}">
                                            {{ csrf_field() }}
                                            <button class="btn btn-success" type="submit">
                                                Upgrade
                                            </button></form></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection

