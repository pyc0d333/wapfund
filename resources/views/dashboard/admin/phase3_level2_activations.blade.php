@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection


@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-sm-12 portlets sortable">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header">
                    <h1 class="custom-font"><strong>Phase 3</strong> Level 2 Users</h1>
                    <p class="alert alert-info">Recycle Accounts </p>
                    <ul class="controls">
                        <li class="dropdown">

                            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                                <i class="fa fa-spinner fa-spin"></i>
                            </a>

                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                <li>
                                    <a role="button" tabindex="0" class="tile-toggle">
                                        <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                        <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-refresh">
                                        <i class="fa fa-refresh"></i> Refresh
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-fullscreen">
                                        <i class="fa fa-expand"></i> Fullscreen
                                    </a>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body p-0">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Transaction Hash</th>
                            <th>Recycle</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <?php
                                    $tx_hash = App\Transaction::where('user_id', $user->id)->where('level', 2)->where('phase', 3)->orderBy('created_at', 'DESC')->first();
                                    $tx_hash = $tx_hash->tx_hash;
                                    ?>
                                    {{$tx_hash}}</td>
                                <td><form  method="post" action="{{route('recycleAccount', $user->id)}}">
                                        {{ csrf_field() }}
                                        <button class="btn btn-success" type="submit">
                                            Recycle Account
                                        </button></form></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
    </div>

@endsection