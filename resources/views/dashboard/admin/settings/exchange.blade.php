@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <h6 class="text-center">Add Exchange</h6>
            <form method="POST" action="{{ route('save-exchange')}}" name="form" class="form-horizontal form-validation mt-20">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Country Name</label>
                    <input type="text" class="form-control underline-input"  name="title" id="title">
                </div>

                <div class="form-group">
                    <label for="link">Site Link</label>
                    <input type="url" class="form-control"  name="link" id="link">
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>

        <div class="col-md-5">
            <br><br>
            @include('flash::message')
            <ul>

                @foreach($exchanges as $exchange)
                <li>{{$exchange->title}}</li>
                    @endforeach
            </ul>
        </div>
    </div>

@endsection