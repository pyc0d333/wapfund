@extends('layouts.dashboard')
@section('title')
    <title>{{ config('app.name', 'Wapfunds') }}</title>
@endsection
@section('content')
    <?php

            $sponsorId = Auth::user()->referrer;
            $user= Auth::user();

            if(Auth::user()->level == 1)
                {
                    $sponsor = App\User::find($sponsorId);

                    if($sponsor->loan_account || $sponsor->level != 1 || !$sponsor->is_activated)
                    {
                        $sponsor = App\User::find(1);

                    }


                }

                if(Auth::user()->level == 0 && Auth::user()->phase == 0)
                {
                    $sponsor = App\User::find($sponsorId);

                    if($sponsor->loan_account || $sponsor->level != 1 || !$sponsor->is_activated)
                    {
                        $sponsor = App\User::find(1);

                    }

                }



                if(Auth::user()->level == 1 && Auth::user()->phase == 1)
                {
                    $sponsor = App\User::find($sponsorId);


                    if($sponsor->loan_account || $sponsor->level != 2 || !$sponsor->is_activated)
                    {
                        $sponsor = App\User::find(1);

                    }

                }

                if(Auth::user()->level == 2 && Auth::user()->phase == 1)
                {
                    $sponsor = App\User::find($sponsorId);

                    if($sponsor->id = 1)
                        {
                            $sponsor = App\User::find($sponsorId);

                        }

                    if($sponsor->loan_account || $sponsor->level != 1 || !$sponsor->is_activated)
                    {
                        $sponsor = App\User::find(1);

                    }

                }

            else
                {
                    $sponsor = App\User::find($sponsorId);
                    $sponsorId = $sponsor->referrer;
                    $sponsor = App\User::find($sponsorId);

                    if($sponsor->loan_account || $sponsor->level != 1 || !$sponsor->is_activated)
                    {
                        $sponsor = App\User::find(1);

                    }

                }
             ?>

    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Upgrade Level for<strong class="txt-primary"> {{$user->username}}</strong></h6>
                        @if($user->level != 1)
                            <p class="alert alert-danger">This account is  maxed out account and needs to make a new payment to Recycle</p>

                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <span class="font-12 mb-5"><strong>Level</strong></span>
                                    <p class="txt-dark">{{$user->level}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <span class="font-12 mb-5"><strong>Sponsor Name</strong></span>
                                    <p  class="txt-dark">{{$sponsor->name}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-success">
                                <div class="sl-content">
                                    <span class="font-12 mb-5">Sponsor Email</span>
                                    <p  class="txt-dark">{{$sponsor->email}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-warning">
                                <div class="sl-content">
                                    <span class="font-12 mb-5">Sponsor Phone</span>
                                    <p  class="txt-dark">{{$sponsor->phone}}</p>
                                </div>
                            </div>

                            <div class="sl-item sl-warning">
                                <div class="sl-content">
                                    <span class="font-8 mb-10">Sponsor BTC Address</span>
                                    <span class="txt-danger" style="word-wrap: break-word;"><strong>{{$sponsor->btc_address}}</strong></span>
                                </div>
                            </div>
                            @if($user->level == 1 && $user->phase == 1 )
                                <form class="form-horizontal form-validation mt-20"  name="form"  role="form" method="POST" action="{{ route('phaseOneLevelTwoTransaction', [$user->id, $sponsor->id]) }}">

                                    @else
                                        <form class="form-horizontal form-validation mt-20"  name="form"  role="form" method="POST" action="{{ route('recycleAccountTransaction', [$user->id, $sponsor->id]) }}">
                                            @endif
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('tx_hash') ? ' has-error' : '' }}">
                                                <label for="wallet_id" class="col-md-3 control-label">Transaction Hash</label>
                                                <div class="col-md-9 col-sm-12">
                                                    <input id="wallet_id" type="text" class="form-control underline-input" name="tx_hash" value="{{ old('tx_hash') }}" required autofocus>

                                                    @if ($errors->has('tx_hash'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('tx_hash') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('amount_paid') ? ' has-error' : '' }}">
                                                <label for="amount" class="col-md-3 control-label">Amount Paid</label>

                                                <div class="col-md-9 col-sm-12">
                                                    <input id="amount" type="number" class=" underline-input form-control" name="amount_paid" required>

                                                    @if ($errors->has('amount_paid'))
                                                        <span class="help-block">
                            <strong>{{ $errors->first('amount_paid') }}</strong>
                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            @if(!$user->awaiting_payment_confirmation)
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        @if($user->level == 1 && $user->phase == 1 )

                                                            <button type="submit" class="btn btn-rounded btn-primary btn-block btn-sm">Submit Payment</button>
                                                        @else
                                                            <button type="submit" class="btn btn-rounded btn-primary btn-block btn-sm">Recycle My Account</button>

                                                        @endif
                                                    </div>
                                                </div>
                                            @else
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <p class="label label-warning">You have already requested payment confirmation</p>
                                                    </div>
                                                </div>

                                            @endif

                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

