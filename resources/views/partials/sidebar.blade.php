
<!-- ================================================
================= SIDEBAR Content ===================
================================================= -->
<?php
        $user = Auth::user();
        ?>
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">

            @if(!$user->is_superadmin)

            <li>
                <a   href="{{url('/home')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-home mr-10"></i>Home<span class="pull-right"></span></a>
            </li>
            @if(Auth::user()->is_activated == true && Auth::user()->awaiting_payment_confirmation == false || $user->level > 0)

            <li>
                <a   href="{{route('upgrade')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-universal-access mr-10"></i>Upgrade<span class="pull-right"></span></a>
            </li>
            @elseif(!$user->is_activated)

            <li>
                <a   href="{{route('activate_account')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-flash mr-10"></i>Activate Account<span class="pull-right"></span></a>
            </li>
            @endif
            @if($user->is_activated)

                <li>
                    <a   href="{{url('ref-link')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-chain mr-10"></i>Referral Link<span class="pull-right"></span></a>
                </li>
                @else
            @endif
            <li>
                <a   href="{{route('downlines')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-users mr-10"></i>Downlines<span class="pull-right"></span></a>
            </li>

            <li>
                <a   href="{{route('pub-exchanges')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-bitcoin mr-10"></i>Buy/Sell BTC<span class="pull-right"></span></a>
            </li>
            <li class="">
                <a href="{{ url ('testimonial')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-comments mr-10"></i>Testimonial</a>
            </li>

            @else
                @endif

            @if($user->is_superadmin)

            <li>
                <a   href="{{route('list-users')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-users mr-10"></i>Users<span class="pull-right"></span></a>
            </li>
            <li>
                <a   href="{{route('activations')}}"><i class="fa fa-flash mr-10"></i>Activations<span class="badge badge-danger pull-right">{{$activations->count()}}</span></a>
            </li>
            <li><a href="{{route ('phase1_level1_activations')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-users"></i>Level 1  <span class="badge badge-danger pull-right">{{$level1->count()}}</span></a></li>
            <li><a href="{{route ('phase1_level2_activations')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-users"></i>Level 2<span class="badge badge-danger pull-right">{{$level2->count()}}</span></a></li>
            <li><a href="{{route('loan_accounts')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-universal-access mr-10"></i>Loan Accounts<span class="pull-right"></span></a></li>

                <li><a href="{{route('all-tickets')}}" data-toggle="collapse" data-target="#dashboard_dr" ><i class="fa fa-universal-access mr-10"></i>Tickets<span class="badge badge-danger pull-right">{{$tickets}}</span></a></li>
                <li class=""><a href="{{route('testimonials') }}" ><i class="fa fa-comments"></i>Testimonials</a></li>

                <li><a   href="{{route('admin.exchanges')}}" data-toggle="collapse" data-target="#dashboard_dr" ><i class="fa fa-bitcoin mr-10"></i>Exchanges<span class="pull-right"></span></a></li>
                <li><a   href="{{route('logs')}}" data-toggle="collapse" data-target="#dashboard_dr" ><i class="fa fa-universal-access mr-10"></i>Logs<span class="pull-right"></span></a></li>
                <li><a href="{{route('tutorials')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-book mr-10"></i>Knowledge Base<span class="pull-right"></span></a></li>
                @else
            @endif

        </ul>
    </div>
<!--/ SIDEBAR Content -->