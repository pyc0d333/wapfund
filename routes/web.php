<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/testimonial', 'TestimonialController@index');
Route::get('/knowledgebase', 'PagesController@tutorials')->name('knowledgebase');
Route::get('/knowledgebase/{id}/details', 'PagesController@tutorial')->name('tut');

Route::get('/', function (){
    return view('home');
});

Route::get('/about', function (){
    return view('pages.about');
});


Route::get('/exchanges', 'PagesController@public_exchanges')->name('pub-exchanges');


Route::get('/privacy', function (){
    return view('pages.privacy');
});

Route::get('/works', function (){
    return view('pages.works');
});

Route::get('/faq', function (){
    return view('pages.faq');
});

Route::get('/ref-link', function (){
    return view('dashboard.ref_link');
});

Route::get('/merchants', function (){
    return view('pages.bitcoin-merchants');
})->name('merchants');


Route::get('/downlines', 'UserController@getDownlines')->name('downlines');

Route::get('/upgrade', 'SiteController@upgrade')->name('upgrade');
Route::get('/upgrade/{id}', 'SiteController@inner_upgrade')->name('inner-upgrade');

Route::get('/successfulverification', 'SiteController@verified')->name('verified');
Route::get('/profile', 'SiteController@profile')->name('profile');

Route::get('/user/{id}', 'UserController@account_detail')->name('account-detail');


//bitcoins
Route::get('/bitcoin_buy', 'SiteController@buy_bitcoin')->name('buy_bitcoin');
Route::get('/bitcoin_sell', 'SiteController@sell_bitcoin')->name('sell_bitcoin');
Route::post('/bitcoin_buy/order/{id}', 'OrderController@buy_bitcoin')->name('bitcoin_order');


//tickets
Route::get('/all_tickets', 'TicketController@index')->name('all-tickets');
Route::post('/resolve_ticket/{ticketId}', 'TicketController@resolve_ticket')->name('resolve_ticket');

Route::get('register/verify/{token}', 'Auth\RegisterController@verify');


Auth::routes();

/**
 * Transaction Routes
 */

Route::post('/phase_one_level_two_transaction/{id}/{sponsorId}', 'TransactionController@phaseOneLevelTwoTransaction')->name('phaseOneLevelTwoTransaction');


Route::post('/phase_two_level_one_transaction/{id}', 'TransactionController@phaseTwoLevelOneTransaction')->name('phaseTwoLevelOneTransaction');
Route::post('/phase_two_level_two_transaction/{id}', 'TransactionController@phaseTwoLevelTwoTransaction')->name('phaseTwoLevelTwoTransaction');
Route::post('/phase_three_level_one_transaction/{id}', 'TransactionController@phaseThreeLevelOneTransaction')->name('phaseThreeLevelOneTransaction');
Route::post('/phase_three_level_two_transaction/{id}', 'TransactionController@phaseThreeLevelTwoTransaction')->name('phaseThreeLevelTwoTransaction');
Route::post('/recycle_transaction/{id}/{sponsorId}', 'TransactionController@recycleAccountTransaction')->name('recycleAccountTransaction');


Route::get('/users', 'UserController@index')->name('users');
Route::get('/paybtc/{id}', 'UserController@PayToBTCWallet')->name('payBTC');
Route::post('/activate/{id}', 'ActivateAccountController@activate')->name('activate');
Route::get('/activate/emailActivation/{id}', 'ActivateAccountController@activateFromEmail')->name('activateFromEmail');
Route::get('/activate/account', 'ActivateAccountController@index')->name('activate_account');
Route::post('/transaction/{id}/{sponsorId}', 'TransactionController@transaction')->name('new_transaction');
Route::post('/ticket/{id}', 'TicketController@save')->name('new_ticket');
Route::post('add_btc_address/{id}', 'UserController@update')->name('add_btc_address');
Route::post('testimonial/{id}', 'TestimonialController@save')->name('add_testimonial');

Route::get('awaiting/', 'SiteController@awaiting_confirmation')->name('awaiting_confirmation');


Route::post('/level_two_upgrade/{id}', 'UpgradeToPhaseOneLevelTwoController@upgrade')->name('phase_1_level_2');
Route::get('/level_two_upgrade/emailActivation/{id}', 'UpgradeToPhaseOneLevelTwoController@emailUpgrade')->name('phase_1_level_2_email_upgrade');



Route::post('/level_one_phase_two_upgrade/{id}', 'UpgradeToPhaseTwoLevelOneController@upgrade')->name('phase_2_level_1');
Route::post('/level_two_phase_two_upgrade/{id}', 'UpgradeToPhaseTwoLevelTwoController@upgrade')->name('phase_2_level_2');
Route::post('/level_one_phase_three_upgrade/{id}', 'UpgradeToPhaseThreeLevelOneController@upgrade')->name('phase_3_level_1');
Route::post('/level_two_phase_three_upgrade/{id}', 'UpgradeToPhaseThreeLevelTwoController@upgrade')->name('phase_3_level_2');


Route::post('/recycle_account/{id}', 'RecycleController@recycleAccount')->name('recycleAccount');
Route::get('/recycle_account/emailActivation/{id}', 'RecycleController@recycleAccountEmail')->name('recycleAccountEmail');


/**
 * Dashboard Admin Routes
 */
Route::get('activations', 'AdminController@activations')->name('activations');
Route::get('phase1_level1_activations', 'AdminController@phase1_level1_activations')->name('phase1_level1_activations');
Route::get('phase1_level2_activations', 'AdminController@phase1_level2_activations')->name('phase1_level2_activations');
Route::get('phase2_level1_activations', 'AdminController@phase2_level1_activations')->name('phase2_level1_activations');
Route::get('phase2_level2_activations', 'AdminController@phase2_level2_activations')->name('phase2_level2_activations');
Route::get('phase3_level1_activations', 'AdminController@phase3_level1_activations')->name('phase3_level1_activations');
Route::get('phase3_level2_activations', 'AdminController@phase3_level2_activations')->name('phase3_level2_activations');



Route::get('admin/list/users', 'AdminController@list_users')->name('list-users');
Route::get('admin/list/users/{id}', 'AdminController@user_detail')->name('user-detail');
Route::get('admin/list/users/suspend/{id}', 'AdminController@suspend_account')->name('suspend_account');
Route::get('admin/list/users/restore/{id}', 'AdminController@restore_account')->name('restore_account');


Route::get('admin/logs', 'AdminController@logs')->name('logs');
Route::get('admin/testimonials', 'AdminController@testimonials')->name('testimonials');
Route::get('admin/users/loan_accounts/list', 'AdminController@loan_accounts')->name('loan_accounts');

Route::get('admin/tutorials','TutorialController@index')->name('tutorials');
Route::get('admin/tutorials/add','TutorialController@add')->name('add_tut');
Route::get('admin/tutorials/edit/{id}','TutorialController@edit')->name('edit-tut');
Route::post('admin/tutorials/save','TutorialController@save')->name('save-tut');
Route::post('admin/tutorials/update/{id}','TutorialController@update')->name('update-tut');

Route::get('admin/exchanges/','AdminController@exchanges')->name('admin.exchanges');
Route::post('admin/exchange/save','AdminController@save_exchange')->name('save-exchange');

/**
 * User Confirmations
 */

Route::get('approve/payments', 'PaymentApprovalController@payment_confirmation_requests')->name('payment_confirmation_requests');
