<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable  =  ['tx_hash', 'amount_paid',  'level', 'phase'];



    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
