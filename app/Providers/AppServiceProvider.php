<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Testimonial;
use App\Ticket;

class AppServiceProvider extends ServiceProvider
{

    public function __construct()
    {

    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);
        $activations = User::where('is_activated', false)->where('awaiting_payment_confirmation', true)->get();

        $tickets = Ticket::all()->count();
        $testimonials = Testimonial::all()->count();
        $level1 = User::where('level', 1)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();
        $level2 = User::where('level', 2)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();

        View::share('activations', $activations);
        View::share('tickets', $tickets);
        View::share('testimonials', $testimonials);
        View::share('level1', $level1);
        View::share('level2', $level2);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
