<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tutorial;

class TutorialController extends Controller
{
    public function __construct(Tutorial $tutorial)
    {
        $this->middleware('auth');
        $this->tutorial = $tutorial;
    }

    public function index()
    {

        $tuts = Tutorial::all();

        return view('dashboard.admin.tutorials.index',compact('tuts', $tuts));
    }

    public function add()
    {
        return view('dashboard.admin.tutorials.add');
    }

    public function edit($id)
    {
        $tut = $this->tutorial->find($id);
        return view('dashboard.admin.tutorials.edit', compact('tut',$tut));
    }

    public function save(Request $request)
    {
        $tut = new Tutorial();
        $tut->title = $request->input('title');
        $tut->content = $request->input('content');
        $tut->save();
        flash('Data updated successfully', 'success');
        return redirect()->route('tutorials');
    }


    public function update($id)
    {
        $tut = $this->tutorial->find($id);
        $tut->save();
        flash('Data updated successfully', 'success');
        return redirect()->route('tutorials');
    }


}
