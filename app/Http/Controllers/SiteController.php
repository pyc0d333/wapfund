<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Order;
use App\User;
use Auth;

class SiteController extends Controller
{

    protected $user;
    protected $order;

    public function __construct(User $user, Order $order)
    {
        $this->middleware('auth');
        $this->order = $order;
        $this->user = $user;
    }


    public function index()
    {

        $accounts = $this->user->where('email', Auth::user()->email)->all();

        if(Auth::check())
        {

            flash('you are already logged in ', 'danger');
            return redirect()->route('profile');
        }

        else
        {
            return view('dashboard.home')->with(['accounts'=>$accounts]);

        }

    }


    public function profile()
    {

        return view('dashboard.profile');
    }

    public  function verified()
    {
        $this->addSponsor();
       return view('auth.verified');
    }


    public  function upgrade()
    {
        if(Auth::user()->is_activated == false)
        {
            return redirect()->route('home');
        }

        if(Auth::user()->btc_address == '')
        {
            flash('you need to add your bitcoin address before you can start using this platform', 'danger');
            return redirect()->route('profile');
        }

        else
        {
            return view('dashboard.upgrade');

        }
    }

    public  function inner_upgrade($id)
    {

        $user = $this->user->find($id);

        if($user->is_activated == false)
        {
            return redirect()->route('home');
        }


        else
        {
            return view('dashboard.inner-upgrade')->with('user',$user);

        }
    }


    /**
     * attach user to sponsor
     */

    public function addSponsor()
    {
        $user = Auth::user();
        $referred_by = $user->referrer;

        $sponsor = User::findOrFail($referred_by);
        if($sponsor->left == 0)
        {
            $sponsor->left = $user->id;
            $sponsor->save();
        }

        else
        {
            if($sponsor->right == 0)
            {
                $sponsor->right = $user->id;
                $sponsor->save();
            }

            else
            {
                $user->referrer = 1;
                $user->save();
            }
        }
    }



    public function buy_bitcoin()
    {
        $orders = $this->order->where('user_id', Auth::user()->id)->get();

        if(!$orders){
            $orders = "NO Orders";
        }
        return view('dashboard.bitcoins.buy')->with('orders',$orders);
    }
    public function sell_bitcoin()
    {
        $orders = $this->order->where('user_id', Auth::user()->id)->get();

        if(!$orders){
            $orders = "NO Orders";
        }
        return view('dashboard.bitcoins.sell')->with('orders',$orders);
    }


    public function awaiting_confirmation()
    {
        return view('dashboard.awaiting_confirmation');
    }
}

