<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class UpgradeToPhaseThreeLevelTwoController extends Controller
{

    protected $user;
    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user = $user;
    }


    public  function upgrade(Request $request, $id)
    {
        $user = $this->user->find($id);
        $user->is_activated = true;
        $user->awaiting_payment_confirmation = false;
        $user->phase = 3;
        $user->level = 2;
        $user->payments += 1600;
        $user->save();
        $sponsor_id = $user->referrer;
        $sponsor = $this->user->find($sponsor_id)->first();
        $sponsor->earnings += 1600;
        $sponsor->save();
        return redirect()->back();

    }


    public  function getRate()
    {

//        $get_rate = file_get_contents(("https://blockchain.info/tobtc?currency=USD&value=10"), true);
//        $btc_rate = ltrim(($get_rate - floor($get_rate)), "0.");
//        return  $btc_rate;

        return 1600;
    }

}
