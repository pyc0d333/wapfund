<?php

namespace App\Http\Controllers;

use App\Tutorial;
use Illuminate\Http\Request;
use App\Testimonial;
use App\Exchanges;


class PagesController extends Controller
{
    public function __construct(Tutorial $tutorial, Testimonial $testimonial)
    {
        $this->tutorial = $tutorial;
        $this->testimonial = $testimonial;
    }

    public function public_exchanges()
    {
        $exchanges = Exchanges::all();
        return view('pages.exchanges', compact('exchanges', $exchanges));
    }
    public function tutorials()
    {
        $tuts = Tutorial::all();
        return view('pages.knowledbase', compact('tuts', $tuts));
    }

    public function tutorial($id)
    {
        $tut = $this->tutorial->find($id);
        return view('pages.tut', compact('tut', htmlspecialchars($tut)));
    }
}
