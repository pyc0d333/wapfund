<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Transaction;
use Mail;
use App\Log;



class TransactionController extends Controller
{
    protected $user;
    protected $user_id;

    public function __construct(User $user, Request $request)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->request = $request;


    }


    public function transaction(Request $request, $id, $sponsorId)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:25'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');
        
        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 0;
        $transaction->user_id = $id;
        $transaction->phase = 0;
        $transaction->expected_amount = 25;
        $transaction->save();

        $sponsor = $this->user->find($sponsorId);

        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->current_sponsor = $sponsorId;
        $user->save();



        if($user->level == 0)
        {

            Mail::send('emails.transaction_received', ['user' => $user, 'tx_hash'=>$request->input('tx_hash'), 'amount'=>'$25'], function ($m) use ($user) {
                $m->from('support@wapfunds.com', 'Wapfunds');

                $m->to($user->email, $user->name)->subject('Wapfunds Transaction');
            });


            Mail::send('emails.activation', ['user' => $sponsor, 'tx_hash'=>$tx_hash, 'donor'=>$user, 'amount'=>'$25'], function ($m) use ($sponsor) {
                $m->from('support@wapfunds.com', 'Wapfunds');
                $m->to($sponsor->email, $sponsor->name)->subject('Wapfunds   Payments Received');
            });


            $log = new Log();
            $log->action = $user->username . 'requested a payment confirmation';
            $log->user_id = $user->id;
            $log->save();

            return redirect()->route('awaiting_confirmation');
        }


        Mail::send('emails.transaction_received', ['user' => $user, 'tx_hash'=>$request->input('tx_hash'), 'amount'=>'$25'], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->name)->subject('Wapfunds Transaction');
        });


        Mail::send('emails.payment_received', ['user' => $sponsor, 'tx_hash'=>$tx_hash, 'amount'=>'$25'], function ($m) use ($sponsor) {
            $m->from('support@wapfunds.com', 'Wapfunds');
            $m->to($sponsor->email, $sponsor->name)->subject('Wapfunds   Payments Received');
        });


        $log = new Log();
        $log->action = $user->username . 'requested a payment confirmation';
        $log->user_id = $user->id;
        $log->save();

        return redirect()->route('awaiting_confirmation');
    }


    public function phaseOneLevelTwoTransaction(Request $request, $id, $sponsorId)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:25'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');

        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 1;
        $transaction->user_id = $id;
        $transaction->phase = 1;
        $transaction->expected_amount = 25;
        $transaction->save();

        $sponsor = $this->user->find($sponsorId);
        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->current_sponsor = $sponsorId;
        $user->save();

        Mail::send('emails.transaction_received', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->nauser)->subject('Wapfunds Transaction');
        });

        Mail::send('emails.regular_payment', ['user' => $sponsor, 'tx_hash'=>$tx_hash, 'donor'=>$user, 'amount'=>'$25'], function ($m) use ($sponsor) {
            $m->from('support@wapfunds.com', 'Wapfunds');
            $m->to($sponsor->email, $sponsor->name)->subject('Wapfunds   Payments Received');
        });


        $log = new Log();
        $log->action = $user->username . 'requested a payment confirmation';
        $log->user_id = $user->id;
        $log->save();

        return redirect()->route('awaiting_confirmation');
    }


    public function recycleAccountTransaction(Request $request, $id, $sponsorId)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:25'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');

        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 2;
        $transaction->user_id = $id;
        $transaction->phase = 1;
        $transaction->expected_amount = 25;
        $transaction->save();


        $sponsor = $this->user->find($sponsorId);


        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->current_sponsor = $sponsorId;

        $user->save();



        Mail::send('emails.transaction_received', ['user' => $user, 'sponsor'=>$sponsor], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->u)->subject('Wapfunds Transaction');
        });



        Mail::send('emails.recycle_payment', ['user' => $sponsor, 'tx_hash'=>$tx_hash, 'donor'=>$user, 'amount'=>'$25'], function ($m) use ($sponsor) {
            $m->from('support@wapfunds.com', 'Wapfunds');
            $m->to($sponsor->email, $sponsor->name)->subject('Wapfunds   Payments Received');
        });


        $log = new Log();
        $log->action = $user->username . 'requested a payment confirmation';
        $log->user_id = $user->id;
        $log->save();

        return redirect()->route('awaiting_confirmation');
    }
































    public function phaseTwoLevelOneTransaction(Request $request, $id)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:140'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');

        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 1;
        $transaction->user_id = $id;
        $transaction->phase = 2;
        $transaction->expected_amount = 140;
        $transaction->save();

        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->save();


        Mail::send('emails.transaction_received', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->nauser)->subject('Wapfunds Transaction');
        });

        return redirect()->route('awaiting_confirmation');
    }


    public function phaseTwoLevelTwoTransaction(Request $request, $id)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:250'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');

        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 2;
        $transaction->user_id = $id;
        $transaction->phase = 1;
        $transaction->expected_amount = 250;
        $transaction->save();


        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->save();


        Mail::send('emails.transaction_received', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->nauser)->subject('Wapfunds Transaction');
        });

        return redirect()->route('awaiting_confirmation');
    }

    public function phaseThreeLevelOneTransaction(Request $request, $id)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:900'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');

        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 1;
        $transaction->user_id = $id;
        $transaction->phase = 3;
        $transaction->expected_amount = 900;
        $transaction->save();


        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->save();


        Mail::send('emails.transaction_received', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->nauser)->subject('Wapfunds Transaction');
        });

        return redirect()->route('awaiting_confirmation');
    }



    public function phaseThreeLevelTwoTransaction(Request $request, $id)
    {
        $this->validate($this->request, [
            'tx_hash'=>'required|min:64',
            'amount_paid' => 'required|numeric|size:1600'
        ]);

        $tx_hash = $request->input('tx_hash');
        $amount_paid = $request->input('amount_paid');

        $transaction = new Transaction();
        $transaction->tx_hash = $tx_hash;
        $transaction->amount_paid = $amount_paid;
        $transaction->level = 2;
        $transaction->user_id = $id;
        $transaction->phase = 3;
        $transaction->expected_amount = 1600;
        $transaction->save();


        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = true;
        $user->save();


        Mail::send('emails.transaction_received', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->nauser)->subject('Wapfunds Transaction');
        });

        return redirect()->route('awaiting_confirmation');
    }




}
