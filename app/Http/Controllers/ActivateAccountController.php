<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Mail;
use Log;

class ActivateAccountController extends Controller
{


    protected $user;
    protected $user_id;
//
    public function __construct(Request $request, User $user)
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->user = $user;
     }


     public function index()
     {

         if(Auth::user()->btc_address == '')
         {
             flash('you need to add your bitcoin address before you can start using this platform', 'danger');
             return redirect()->route('profile');
         }

         return view('dashboard.activate');
     }


     public function transaction(Request $request, $id)
     {
         $this->validate($this->request, [
             'tx_hash'=>'required|min:64',
             'amount_paid' => 'required|numeric|size:25'
         ]);

         $tx_hash = $request->input('tx_hash');
         $amount_paid = $request->input('amount_paid');


         $transaction = new Transaction();
         $transaction->tx_hash = $tx_hash;
         $transaction->amount_paid = $amount_paid;
         $transaction->level = 0;
         $transaction->phase = 0;
         $transaction->expected_amount = 25;
         $transaction->save();
     }
    //
    /** @var
     * pay $25 to referrer blockchain account to activate account
     * then upgrade payee to Phase 1 level 1 and make account active
     * User submits transaction hash  and amount paid for admin to review and confirm the payment.
     * The user account is placed on hold from performing any transactions until the order is approved
     * @return user,
     **/
    public function activate(Request $request, $id)
    {

            $user = $this->user->find($id);
            $user->is_activated = true;
            $user->awaiting_payment_confirmation = false;
            $user->phase = 1;
            $user->level = 1;
            $user->help_count += 1;
            $user->payments += 25;
            $user->save();

            $sponsor_id = $user->current_sponsor;
            $sponsor = $this->user->find($sponsor_id);
            $sponsor->earnings += 25;
            $sponsor->received_count += 1;
            $sponsor->save();

            $referrer = $this->user->find($user->referrer);

            if($referrer->loan_account)
            {
                $referrer->help_count += 1;
                $referrer->is_activated = true;
                $referrer->save();

                if($referrer->help_count > 1)
                {
                    $referrer->loan_account = false;
                    $referrer->level = 2;
                    $referrer->phase = 1;
                    $referrer->save();
                }
            }

            Mail::send('emails.payment_approved', ['user' => $user], function ($m) use ($user) {
            $m->from('support@bitycom.com', 'Bitycom');

            $m->to($user->email, $user->name)->subject('Payment Approval');
            });

        flash('successfully activated account', 'message');
        return redirect()->route('activations');
    }




    /** @var
     * pay $25 to referrer blockchain account to activate account
     * then upgrade payee to Phase 1 level 1 and make account active
     * User submits transaction hash  and amount paid for admin to review and confirm the payment.
     * The user account is placed on hold from performing any transactions until the order is approved
     * @return user,
     **/
    public function activateFromEmail($id)
    {

            $user = $this->user->find($id);
            $user->is_activated = true;
            $user->awaiting_payment_confirmation = false;
            $user->phase = 1;
            $user->level = 1;
            $user->payments += 25;
            $user->help_count += 1;
            $user->save();


            //get sponsor and update
            $sponsor_id = $user->current_sponsor;
            $sponsor = $this->user->find($sponsor_id);
            $sponsor->received_count += 1;
            $sponsor->earnings += 25;
            $sponsor->save();


            $referrer = $this->user->find($user->referrer);

            if($referrer->loan_account)
            {
                $referrer->help_count += 1;
                $referrer->is_activated = true;
                $referrer->save();

                if($referrer->help_count > 1)
                {
                    $referrer->loan_account = false;
                    $referrer->save();
                }
        }

            Mail::send('emails.payment_approved', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Bitycom');

            $m->to($user->email, $user->name)->subject('Payment Approval');
            });

        flash('successfully confirmed payment', 'message');
        return redirect()->route('upgrade');
    }



    /**
     * Query blockchain info api to get the current rate for upgrade in satoshis
     * return :double which is stripped of the decimal part and the fractional part used as the amount to be paid
     */

    public  function getRate()
    {

//        $get_rate = file_get_contents(("https://blockchain.info/tobtc?currency=USD&value=1"), true);
//        $btc_rate = ltrim(($get_rate - floor($get_rate)), "0.");
//        return  $btc_rate;
         return 25;
    }


    /**
     * @return authenticated user's referrer
     */
    public function getUpline()
    {
        return Auth::user()->referrer;
    }



}
