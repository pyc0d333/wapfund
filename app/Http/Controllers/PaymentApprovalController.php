<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Transaction;
use Illuminate\Support\Facades\Mail;

class PaymentApprovalController extends Controller
{
    protected  $user;

    public function __construct(Request $request, User $user, Transaction $transaction )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->transaction = $transaction;
    }


    public function payment_confirmation_requests()
    {
        $users = $this->user->where('referrer', Auth::user()->id)->where('awaiting_payment_confirmation', true)->get();
        return view('dashboard.payment_confirmations')->with('users', $users);
    }


    public function confirm_payment()
    {
        dd('confirm payment');
    }
}
