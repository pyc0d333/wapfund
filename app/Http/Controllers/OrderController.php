<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Order;


class OrderController extends Controller
{

    protected $user;
    protected $order;

    public function __construct(User $user, Order $order)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->order =$order;
    }

    //save bitcoin buy order
    public function buy_bitcoin(Request $request, $id)
    {
        $this->validate($request,
            [
                'order_amount'=>'numeric|required',
                'amount_paid'=>'numeric|required',
                'payment_method'=>'required',
                'trans_id'=>'required'
            ]);

            $order = new Order();

            $order->user_id = $id;
            $order->order_amount = $request->input('order_amount');
            $order->btc_address = $request->input('btc_address');
            $order->amount_paid = $request->input('amount_paid');
            $order->payment_method = $request->input('payment_method');
            $order->trans_id = $request->input('trans_id');
            $order->status = "pending";
            $order->save();

            flash('Your Order Has been received successfully', 'success');
            return redirect()->back();

    }

}
