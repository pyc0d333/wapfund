<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Log;


class UpgradeToPhaseOneLevelTwoController extends Controller
{
    protected $user;


    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user = $user;
    }


    public  function upgrade(Request $request, $id)
    {
        $user = $this->user->find($id);
        $user->is_activated = true;
        $user->awaiting_payment_confirmation = false;
        $user->phase = 1;
        $user->level = 2;
        $user->payments += 25;
        $user->help_count += 1;
        $user->save();



        $sponsor_id = $user->current_sponsor;
        $sponsor = $this->user->find($sponsor_id);
        $sponsor->earnings += 25;
        $sponsor->received_count += 1;
        $sponsor->save();



        $log = new Log();
        $log->action = $user->username . 'payment was confirmed by' . Auth::user()->username;
        $log->user_id = $user->id;
        $log->save();

        return redirect()->route('phase1_level1_activations');

    }

    public  function emailUpgrade(Request $request, $id)
    {
        $user = $this->user->find($id);
        $user->is_activated = true;
        $user->awaiting_payment_confirmation = false;
        $user->phase = 1;
        $user->level = 2;
        $user->help_count += 1;
        $user->payments += 25;
        $user->save();


        $sponsor_id = $user->current_sponsor;
        $sponsor = $this->user->find($sponsor_id);
        $sponsor->earnings += 25;
        $sponsor->received_count += 1;
        $sponsor->save();

        if($sponsor->earnings >= 50)
        {
            $sponsor->loan_account = false;
            $sponsor->is_activated = true;
            $sponsor->save();
        }

        return redirect()->route('upgrade');

    }


    public  function getRate()
    {

//        $get_rate = file_get_contents(("https://blockchain.info/tobtc?currency=USD&value=10"), true);
//        $btc_rate = ltrim(($get_rate - floor($get_rate)), "0.");
//        return  $btc_rate;

        return 40;
    }


}
