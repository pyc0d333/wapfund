<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function __construct(User $user, Request $request, Ticket $tickect )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->ticket = $tickect;

    }



    public function index()
    {
        $resolved = $this->ticket->where('resolved', true)->get();
        $unresolved = $this->ticket->where('resolved', false)->get();
        $all = $this->ticket->all();


        return view('dashboard.admin.tickets', compact('all', $all));
    }

    public function resolve_ticket($ticketId)
    {
        $ticket = $this->ticket->find($ticketId);
        $ticket->resolved = true;
        $ticket->save();
        flash('Ticket resolved', 'success');
        return redirect()->back();
    }


    public function save(Request $request, $id)
    {

        $this->validate($request, [
            'message' => 'required|min:30',
        ]);

        $user_phone = $this->user->find($id);
        $user_phone = $user_phone->phone;

        $phone = $request->input('phone');

        if ($phone == null) {
            $phone = $user_phone;
        }


        $ticket = new Ticket();
        $ticket->message = $request->input('message');
        $ticket->user_id = $id;
        $ticket->phone = $phone;
        $ticket->save();
        flash('Your message has been received, we will get back to you shortly', 'success');

        $tickets = $this->ticket->where('user_id', Auth::user()->id)->get();
        return redirect()->route('profile')->with('tickets', $tickets);
    }
}
