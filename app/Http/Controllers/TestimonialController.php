<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Testimonial;
use App\Log;

class TestimonialController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user = $user;
    }


    public function index()
    {
        return view('dashboard.testimonial');
    }


    public function save(Request $request, $id)
    {

        $this->validate($request, [
            'testimony'=>'required|min:100',
         ]);

        $testimonial = new Testimonial();
        $testimonial->testimony = $request->input('testimony');
        $testimonial->user_id = $id;
        $testimonial->save();



        $user = $this->user->find($id);

        $log = new Log();
        $log->action = $user->username . "  "  . ' added a testimonial';
        $log->user_id = $user->id;
        $log->save();

        flash('Thank you for submmiting a testimonial', 'success');
        return redirect()->route('profile');

    }
}
