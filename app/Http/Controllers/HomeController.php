<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = $this->user->where('email', Auth::user()->email)->get();

        if(Auth::user()->btc_address == '')
        {
            flash('you need to add your bitcoin address before you can start using this platform', 'danger');
            return redirect()->route('profile');
        }

//        elseif(Auth::user()->awaiting_payment_confirmation)
//        {
//            return redirect()->route('awaiting_confirmation');
//        }

        elseif(Auth::user()->is_admin)
        {
            return redirect()->route('profile');
        }


        else
        {
            return view('dashboard.home')->with(['accounts'=>$accounts]);

        }
    }
}
