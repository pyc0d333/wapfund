<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    protected $request;

    public function PayToBTCWallet($id, $amount, $wallet_id, $password, $to)
    {
        dd([$id, $amount,  $wallet_id, $password, $to]);
        $fee = "20000";
        $json_url = "http://127.0.0.1:3000/merchant/$wallet_id/payment?fee=$fee&password=$password&from=0&to=$to&amount=$amount";
        $json_data = file_get_contents(($json_url), true);
        $json_feed = json_decode($json_data);
        $message = $json_feed->message;
        $txid = $json_feed->tx_hash;
//        return view('dashboard.upgrade')->with('message', $message);
        return $txid;
    }



//    public function getSponsor()
//    {
//        return Auth::user()->referrer;
//    }


    public function checkAddress($address)
    {
        $origbase58 = $address;
        $dec = "0";

        for ($i = 0; $i < strlen($address); $i++)
        {
            $dec = bcadd(bcmul($dec,"58",0),strpos("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",substr($address,$i,1)),0);
        }

        $address = "";

        while (bccomp($dec,0) == 1)
        {
            $dv = bcdiv($dec,"16",0);
            $rem = (integer)bcmod($dec,"16");
            $dec = $dv;
            $address = $address.substr("0123456789ABCDEF",$rem,1);
        }

        $address = strrev($address);

        for ($i = 0; $i < strlen($origbase58) && substr($origbase58,$i,1) == "1"; $i++)
        {
            $address = "00".$address;
        }

        if (strlen($address)%2 != 0)
        {
            $address = "0".$address;
        }

        if (strlen($address) != 50)
        {
            return false;
        }

        if (hexdec(substr($address,0,2)) > 0)
        {
            return false;
        }

        return substr(strtoupper(hash("sha256",hash("sha256",pack("H*",substr($address,0,strlen($address)-8)),true))),0,8) == substr($address,strlen($address)-8);
     }




    /**if sponsor legs are maxed, make superadmin default sponsor
     * @return mixed
     */

    public  function connectToSponsor()
    {


        $user_id = Auth::user()->id;
        $sponsorId =  Auth::user()->referrer;
        $sponsor = $this->user->find($sponsorId);

        if (Auth::user()->is_admin)
        {
            $sponsor->referrer = 1;
            $sponsor->save();
        }

        if (Auth::user()->is_superadmin)
        {

            return Auth::user();
        }

        if ($sponsor->left == 0 && $sponsor->right == 0)
        {
            $sponsor->left = $user_id;
            $sponsor->save();
            return Auth::user();
        }

        if ($sponsor->left != 0 && $sponsor->right == 0)
        {
            $sponsor->right = $user_id;
            $sponsor->save();
            return Auth::user();
        }

        if ($sponsor->left != 0 && $sponsor->right != 0)
        {
            $sponsor->right = $user_id;
            $sponsor->save();
            return Auth::user();
        }

        else
        {
            $me = $this->user->find($user_id);
            $me->referrer = 1;
            $me->save();
            return Auth::user();
        }



    }

    public function addDownlines()
    {
        $myId = Auth::user()->id;
        $me = Auth::user();
        $downlines = $this->user->where('referrer', $myId)->where('is_activated', true)->take(2);

        foreach ($downlines as $downline)
        {
            if($me->left == 0);
        }
        dd($downlines);



    }
}
