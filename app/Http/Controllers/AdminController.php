<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use Auth;
use App\Log;
use App\Testimonial;
use Mail;
use App\Exchanges;
use App\Setting;



class AdminController extends Controller
{
    protected  $user;

    public function __construct(Request $request, Testimonial $testimonial, Log $log, User $user, Transaction $transaction, Exchanges $exchanges )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->log = $log;
        $this->testimonial = $testimonial;
        $this->transaction = $transaction;
        $this->exchange = $exchanges;
    }

    public function exchanges()
    {
        $exchanges = Exchanges::all();
        return view('dashboard.admin.settings.exchange', compact('exchanges', $exchanges));
    }


    public function save_exchange(Request $request)
    {
        $exchange = new Exchanges();
        $exchange->title  = $request->input('title');
        $exchange->link = $request->input('link');
        $exchange->save();

        flash('Added successfully', 'success');

        return redirect()->route('admin.exchanges');
    }

    public function update_exchange(Request $request, $id)
    {
        $exchange = $this->exchange->find($id);
        $exchange->title = $request->input('title');
        $exchange->link = $request->input('link');
        $exchange->save();
        flash('Updated successfully', 'success');
        return redirect()->route('admin.exchanges');
    }




    public function logs()
    {
        $logs = $this->log->latest()->paginate(10);
        return view('dashboard.admin.logs')->with('logs', $logs);
    }

    public function activations()
    {
        $nosers = $this->user->where('is_admin', false)->where('level', 1)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();
        $users = $this->user->where('is_activated', false)->where('awaiting_payment_confirmation', true)->get();
        return view('dashboard.admin.activations')->with(['users'=>$users, 'nosers'=>$nosers]);
    }

    public function list_users()
    {
        $users = $this->user->where('is_superadmin',false)->paginate(10);
        return view('dashboard.admin.list_users')->with('users', $users);
    }

    public function loan_accounts()
    {
        $users = $this->user->where('loan_account',true)->paginate(10);
        return view('dashboard.admin.loan_accounts')->with('users', $users);
    }

    public function testimonials()
    {
        $testimonials = $this->testimonial->latest()->paginate(10);
        return view('dashboard.admin.testimonials')->with('testimonials', $testimonials);

    }

    public function user_detail($id)
    {
        $user = $this->user->find($id);
        $me = $this->user->find($id);

        $left = $this->user->find($user->left);
        $right = $this->user->find($user->right);

        $leftDownleft = $this->user->find($left->left);
        $leftDownright = $this->user->find($left->right);

        $rightDownleft = $this->user->find($right->left);
        $rightDownright = $this->user->find($right->right);

        $logs = $this->log->where('user_id', $id)->latest()->paginate(10);
        $testimonials = $this->testimonial->where('user_id', $id)->get();

        return view('dashboard.admin.user_detail')->with([
                 'user'=>$user,
                 'me'=>$me,
                 'left'=> $left,
                 'right'=> $right,
                 'leftDownleft'=>$leftDownleft,
                 'leftDownright'=>$leftDownright,
                 'rightDownleft'=>$rightDownleft,
                 'rightDownright'=>$rightDownright,
                 'testimonials'=>$testimonials,
                 'logs'=>$logs
                ]
           );
    }


    public function restore_account($id)
    {
        $user = $this->user->find($id);
        $user->suspended = false;

        $user->email = $user->old_email;
        $user->password = $user->old_password;
        $user->btc_address = $user->old_btc_address;
        $user->save();


//        $user->save();


        $log = new Log();
        $log->action = $user->username . " " . ' was Unsuspended by' . " ". Auth::user()->name;
        $log->user_id = $user->id;
        $log->save();


        flash('Account has been successfully restored', 'success');
        return redirect()->back();


    }


    public function suspend_account($id)
    {
        $user = $this->user->find($id);
        $user->suspended = true;

        $admin = $this->user->find(1);

        $log = new Log();
        $log->action = $user->username . " " . "with email "  . $user->email . "suspended " . " " . Auth::user()->name;
        $log->user_id = $user->id;
        $log->save();

        $log = new Log();
        $log->action = $user->username . " " . "with btc_address "  . $user->btc_address . "suspended " . " " . Auth::user()->name;
        $log->user_id = $user->id;
        $log->save();


        //change user information

        $user->email = $admin->email;
        $user->password = Bcrypt('suspended');
        $user->btc_address = $admin->btc_address;
        $user->save();

        $left  =  $this->user->find($user->left);
        $right = $this->user->find($user->right);

//        if($left)
//        {
//            $left->referrer = 1;
//            $left->save();
//        }
//
//        if($right)
//        {
//            $right->referrer = 1;
//            $right->save();
//        }


        Mail::send('emails.suspend', ['user' => $user], function ($m) use ($user) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($user->email, $user->u)->subject('Account Suspension');
        });


        Mail::send('emails.suspend', ['user' => $user, 'admin'=>$admin], function ($m) use ($admin) {
            $m->from('support@wapfunds.com', 'Wapfunds');

            $m->to($admin->email, $admin->u)->subject('Account Suspension');
        });


        $log = new Log();
        $log->action = $user->username . " "  . ' was suspended by' . " " . Auth::user()->name;
        $log->user_id = $user->id;
        $log->save();


        flash('User has been successfully suspended', 'success');
        return redirect()->back();
    }





    public function phase1_level1_activations()
    {
        $users = $this->user->where('level', 1)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();
        $nosers = $this->user->where('is_admin', false)->where('level', 1)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();
        return view('dashboard.admin.phase1_level1_activations')->with(['users'=>$users, 'nosers'=>$nosers]);
    }

    public function phase1_level2_activations()
    {

        $nosers = $this->user->where('is_admin', false)->where('level', 1)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();
        $users = $this->user->where('level', 2)->where('phase', 1)->where('awaiting_payment_confirmation', true)->get();
         return view('dashboard.admin.phase1_level2_activations')->with(['users'=>$users, 'nosers'=>$nosers]);
    }

    public function phase2_level1_activations()
    {
        $users = $this->user->where('level', 1)->where('phase', 2)->where('awaiting_payment_confirmation', true)->get();
         return view('dashboard.admin.phase2_level1_activations')->with('users', $users);
    }


    public function phase2_level2_activations()
    {
        $users = $this->user->where('level', 2)->where('phase', 2)->where('awaiting_payment_confirmation', true)->get();
         return view('dashboard.admin.phase2_level2_activations')->with('users', $users);
    }

    public function phase3_level1_activations()
    {
        $users = $this->user->where('level', 1)->where('phase', 3)->where('awaiting_payment_confirmation', true)->get();
         return view('dashboard.admin.phase3_level1_activations')->with('users', $users);
    }


    public function phase3_level2_activations()
    {
        $users = $this->user->where('level', 2)->where('phase', 3)->where('awaiting_payment_confirmation', true)->get();
         return view('dashboard.admin.phase3_level2_activations')->with('users', $users);
    }


}
