<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Controllers\Session;
use App\Log;
use App\Testimonial;

class UserController extends Controller
{

    protected $user;
    protected $user_id;
    protected $me;
//
    public function __construct(Request $request, Log $log, Testimonial $testimonial, User $user, Auth $me)
    {
        $this->middleware('auth');
        $this->me = $me;
        $this->request = $request;
        $this->user = $user;
        $this->log = $log;
        $this->testimonial = $testimonial;
    }

    /**
     * get all downlines for the logged in user
     * and also display the user's referrer
     * @return $this
     *
     * downline get all users where referrer = id of logged in user
     */
    public function getReferrer()
    {

        $referrer = Auth::user()->referrer;
        $referrer = User::find($referrer);
        $user = Auth::user()->id;
        $downlines = User::all()->where('referrer', $user);
        dd($downlines);
        return view('dashboard.profile')->with([
            'ref' => $referrer,
            'downlines' => $downlines
        ]);
    }




    //get Id of this user on a left leg
    public function getLeftDownline($id)
    {
          $this->user_id = Auth::user()->id;
          $this->me = $this->user->find($id);
          return $this->me->left;
    }


    //get Id of this user on a right leg
    public function getRightDownline($id)
    {
        $this->user_id = Auth::user()->id;
        $this->me = $this->user->find($id);
        return $this->me->right;
    }

    //add new user to left side of sponsor
    public function addToLeftLeg($id)
    {
        $this->user_id = Auth::user()->id;
        $this->me = $this->user->find($this->user_id);
        $this->me->left = $id;
        $this->me->save();

    }


    // add new user to right side of sponsor
    public function addToRightLeg($id)
    {
        $this->user_id = Auth::user()->id;
        $this->me = $this->user->find($this->user_id);
        $this->me->right = $id;
        $this->me->save();
    }


    public function getUpline()
    {
        return Auth::user()->referrer;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * display all the downlines of the user
     */
    public function getDownlines()
    {

//         $this->addDownlines()


        if(Auth::user()->is_activated == false && !Auth::user()->loan_account)
        {

            return redirect()->route('upgrade');
        }
        else
        {
            $user_id = Auth::user()->id;

            $user = $this->user->find($user_id);
            $left = $this->user->find($user->left);
            $right = $this->user->find($user->right);

            $leftDownleft = $this->user->find($left->left);
            $leftDownright = $this->user->find($left->right);


            $rightDownleft = $this->user->find($right->left);
            $rightDownright = $this->user->find($right->right);

            $admin_downlines = $this->user->where('referrer', 1)->where('id', '!=', 1)->get();

             return view('dashboard.downlines')->with([
                 'user'=>$user,
                 'left'=> $left,
                 'admin_downlines'=> $admin_downlines,
                 'right'=> $right,
                 'leftDownleft'=>$leftDownleft,
                 'leftDownright'=>$leftDownright,
                 'rightDownleft'=>$rightDownleft,
                 'rightDownright'=>$rightDownright
             ]);
        }


    }


    /**
     * get any user registered with your referral code and add them to a leg
     */
    public  function  addUserToSponsorLeg()
    {
        $user_id = Auth::user()->getId();

        $new_referral = $this->user
            ->where('referrer', $user_id)
            ->orderBy('created_at', 'desc')
            ->whereNotIn('id', [$user_id])
            ->latest()
            ->take(1)
            ->first();

        $this->me = $this->user->find($user_id);

        if ($new_referral != null)
        {

                if($this->me->left == 3 )
                {
                    $this->addToLeftLeg(2);
                }

                if($this->me->right == 0)
                {
                    $this->addToRightLeg($new_referral->id);

                }

            }
        }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users  = User::all();
        return view('dashboard.users')->with('users', $users);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($this->request, [
            'btc_address'=>'required',
        ]);


        $user = User::findOrFail($id);
        $input =$this->request->input('btc_address');
        $confirm_address = $this->checkAddress($input);

        if($confirm_address == true)
        {
            $user->btc_address = $input;
            $user->old_btc_address = $input;
            $user->update();
            flash('You have successfully added your bitcoin receiving address', 'success');
            return redirect()->back();
        }


        if($confirm_address == false)
        {

            flash('Sorry this address is wrong', 'danger');
            return redirect()->back();
        }


    }


    /**
      * Query blockchain info api to get the current rate for upgrade in satoshis
      * return :double which is stripped of the decimal part and the fractional part used as the amount to be paid
      */


    public  function getRate()
    {

        $get_rate = file_get_contents(("https://blockchain.info/tobtc?currency=USD&value=1"), true);
        $btc_rate = ltrim(($get_rate - floor($get_rate)), "0.");
        return  $btc_rate;
    }


    public function account_detail($id)
    {
        $user = $this->user->find($id);

        if(Auth::user()->email != $user->email)
        {
            abort(404);
        }
        $me = $this->user->find($id);

        $left = $this->user->find($user->left);
        $right = $this->user->find($user->right);

        $leftDownleft = $this->user->find($left->left);
        $leftDownright = $this->user->find($left->right);

        $rightDownleft = $this->user->find($right->left);
        $rightDownright = $this->user->find($right->right);

        $logs = $this->log->where('user_id', $id)->latest()->paginate(10);
        $testimonials = $this->testimonial->where('user_id', $id)->get();

        return view('dashboard.user_detail')->with([
                'user'=>$user,
                'me'=>$me,
                'left'=> $left,
                'right'=> $right,
                'leftDownleft'=>$leftDownleft,
                'leftDownright'=>$leftDownright,
                'rightDownleft'=>$rightDownleft,
                'rightDownright'=>$rightDownright,
                'testimonials'=>$testimonials,
                'logs'=>$logs
            ]
        );
    }



}
