<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Log;


class RecycleController extends Controller
{

    protected $user;


    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user = $user;
    }

    public function recycleAccount(Request $request, $id)
    {

        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = false;
        $user->help_count += 1;
        $user->recycle_count += 1;
        $user->level = 1;
        $user->phase = 1;
        $user->save();

        $sponsor_id = $user->current_sponsor;
        $sponsor = $this->user->find($sponsor_id);

        $sponsor->earnings += 25;
        $sponsor->received_count += 1;
        $sponsor->save();

        $log = new Log();
        $log->action = $user->username . 'payment was confirmed by' . Auth::user()->username;
        $log->user_id = $user->id;
        $log->save();

        return redirect()->back();

    }


    public function recycleAccountEmail(Request $request, $id)
    {

        $user = $this->user->find($id);
        $user->awaiting_payment_confirmation = false;
        $user->level = 1;
        $user->phase = 1;
        $user->help_count += 1;
        $user->recycle_count += 1;
        $user->save();

        $sponsor_id = $user->current_sponsor;
        $sponsor = $this->user->find($sponsor_id);
        $sponsor->earnings += 25;
        $sponsor->received_count += 1;
        $sponsor->save();

        $log = new Log();
        $log->action = $user->username . 'payment was confirmed by' . Auth::user()->username;
        $log->user_id = $user->id;
        $log->save();

        return redirect()->back();

    }

}
