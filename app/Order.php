<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['trans_id', 'order_amount', 'amount_paid', 'expected_amount', 'status', 'payment_method', 'btc_address'];
}
